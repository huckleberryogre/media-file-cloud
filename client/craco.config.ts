import path = require('path');
import CopyPlugin = require('copy-webpack-plugin');
import type { CracoConfig } from '@craco/types';
const CracoAlias = require('craco-alias');

const config: CracoConfig = {
  style: {
    postcss: {
      env: {
        autoprefixer: {
          cascade: true,
        },
      },
    },
  },
  devServer: {
    devMiddleware: {
      writeToDisk: true,
    },
    open: false,
  },
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: 'tsconfig',
        // baseUrl SHOULD be specified
        // plugin does not take it from tsconfig
        baseUrl: './src',
        // tsConfigPath should point to the file where "baseUrl" and "paths" are specified
        tsConfigPath: './tsconfig.json',
      },
    },
  ],
  webpack: {
    configure: (webpackConfig, { paths }) => {
      paths!.appBuild = webpackConfig.output!.path = path.resolve(
        __dirname,
        '../server/dist/client',
      );
      webpackConfig.resolve!.fallback = {
        process: require.resolve('process/browser'),
      };
      return webpackConfig;
    },
    plugins: {
      add: [
        new CopyPlugin({
          patterns: [
            {
              from: 'public',
              to: path.resolve(__dirname, '../server/dist/client'),
              globOptions: {
                ignore: ['**/index.html'],
              },
            },
          ],
        }),
      ],
    },
  },
};

export default config;
