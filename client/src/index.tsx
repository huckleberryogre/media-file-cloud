import { StrictMode } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { App } from './App';
import { store } from './store';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import CssBaseline from '@mui/material/CssBaseline';
import 'fomantic-ui-css/semantic.min.css';
import './api/interceptors';
import { ICON_VARIANTS as NOTISTACK_ICON_VARIANTS } from './components/Notistack/Notistack';

const root = ReactDOM.createRoot(document.getElementById('root')!);

root.render(
  <StrictMode>
    <CssBaseline />
    <Provider store={store}>
      <SnackbarProvider
        iconVariant={NOTISTACK_ICON_VARIANTS}
        maxSnack={3}
        dense
      >
        <App />
      </SnackbarProvider>
    </Provider>
  </StrictMode>,
);
