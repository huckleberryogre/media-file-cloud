/**
 * Request status.
 */
export enum ERequestStatus {
  IDLE = 'IDLE',
  LOADING = 'LOADING',
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}

/**
 * Application theme (mode).
 */
export enum EAppTheme {
  DARK = 'dark',
  LIGHT = 'light',
}

/**
 * File visibility.
 */
export enum EFileVisibility {
  PUBLIC = 'PUBLIC',
  PRIVATE = 'PRIVATE',
}
