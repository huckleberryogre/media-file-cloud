import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { authSlice } from './slice/auth';
import { filesSlice } from './slice/files';
import { notificationsSlice } from './slice/notifications';
import { featuresSlice } from './slice/users';
import { tagsSlice } from './slice/tags';
import { settingsSlice } from './slice/settings';
import { playerSlice } from './slice/player';

export const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    settings: settingsSlice.reducer,
    files: filesSlice.reducer,
    notifications: notificationsSlice.reducer,
    features: featuresSlice.reducer,
    tags: tagsSlice.reducer,
    player: playerSlice.reducer,
  },
});

export type TAppDispatch = typeof store.dispatch;
export type TAppState = ReturnType<typeof store.getState>;
export type TAppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  TAppState,
  unknown,
  Action<string>
>;
