import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from '@reduxjs/toolkit';
import { TagsAPI } from 'api/tagsAPI';
import { ERequestStatus } from 'enums';
import { TAppState } from 'store';
import { ITag } from 'models';
import { login, logout } from '../auth';

interface ITagsSlice {
  tags: ITag[];
  status: ERequestStatus;
  error?: string | null;
}

const initialState: ITagsSlice = {
  tags: [],
  status: ERequestStatus.IDLE,
  error: null,
};

export const getAllTags = createAsyncThunk('tags/getAll', async () => {
  const response = await TagsAPI.getAll();

  return response.data;
});

export const tagsSlice = createSlice({
  name: 'tags',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllTags.rejected, (state, action) => {
        state.status = ERequestStatus.FAILED;
        state.error = action?.error?.message;
      })
      .addCase(getAllTags.pending, (state) => {
        state.status = ERequestStatus.LOADING;
      })
      .addCase(getAllTags.fulfilled, (state, action) => {
        state.status = ERequestStatus.SUCCESS;
        state.tags = action.payload;
      })
      .addCase(logout, () => initialState)
      .addCase(login.fulfilled, () => initialState);
  },
});

export const selectAllTags = (state: TAppState) => state.tags.tags;
export const selectAllTagNames = createSelector(selectAllTags, (tags) =>
  tags.map((tag) => tag.name),
);
export const selectTagsStatus = (state: TAppState) => state.tags.status;
