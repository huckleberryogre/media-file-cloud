import { INotification } from 'models';
import { createSlice } from '@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit/dist/createAction';
import { getHumanReadableDateFromTimestamp } from 'utilities/getHumanReadableDateFromTimestamp';
import { generateUUID } from 'utilities/generateUUID';
import { TAppState } from 'store';
import { login, logout } from '../auth';

interface INotificationsSlice {
  notifications: INotification[];
}

const initialState: INotificationsSlice = {
  notifications: [],
};

export const notificationsSlice = createSlice({
  name: 'notifications',
  initialState,
  reducers: {
    show: (state, action: PayloadAction<INotification>) => {
      const timestamp = getHumanReadableDateFromTimestamp(Date.now());
      const uuid = generateUUID();

      state.notifications = [
        ...state.notifications,
        { ...action.payload, timestamp, uuid, open: true },
      ];
    },
    hide: (state, action: PayloadAction<INotification['uuid']>) => {
      state.notifications = state.notifications.map((notification) => {
        if (notification.uuid === action.payload) {
          notification.open = false;
        }

        return notification;
      });
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(logout, () => initialState)
      .addCase(login.fulfilled, () => initialState);
  },
});

export const notificationActions = notificationsSlice.actions;
export const notificationsSelector = (state: TAppState) =>
  state.notifications.notifications;
