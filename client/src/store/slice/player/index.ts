import { createSlice } from '@reduxjs/toolkit';
import { TAppState } from 'store';
import { IFileEntry } from 'models';
import { login, logout } from '../auth';

export interface IPlayerSlice {
  isPlaying: boolean;
  currentTrackFileEntry: IFileEntry | null;
  triggerPlayFromStart: number;
}

const initialState: IPlayerSlice = {
  isPlaying: false,
  currentTrackFileEntry: null,
  triggerPlayFromStart: 0,
};

/**
 * Player slice.
 */
export const playerSlice = createSlice({
  name: 'player',
  initialState,
  reducers: {
    setIsPlaying: (state, action) => {
      state.isPlaying = action.payload;
    },
    setCurrentTrackFileEntry: (state, action) => {
      state.currentTrackFileEntry = action.payload;
    },
    playFromStart: (state) => {
      state.triggerPlayFromStart = Date.now();
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(logout, () => initialState)
      .addCase(login.fulfilled, () => initialState);
  },
});

export const { setIsPlaying, setCurrentTrackFileEntry, playFromStart } =
  playerSlice.actions;
export const selectIsPlaying = (state: TAppState) => state.player.isPlaying;
export const selectCurrentTrackFileEntry = (state: TAppState) =>
  state.player.currentTrackFileEntry;
export const selectTriggerPlayFromStart = (state: TAppState) =>
  state.player.triggerPlayFromStart;
