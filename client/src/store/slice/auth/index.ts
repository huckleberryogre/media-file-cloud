import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { TAppState } from 'store';
import { EUserRole } from './enums';
import { AuthAPI, CreateUserDtoClient } from 'api/authAPI';
import { ERequestStatus } from 'enums';
import { getCredentialsFromCookie } from 'utilities/getCredentialsFromCookie';
import Cookie from 'js-cookie';

export interface IUser {
  email: string;
  role: EUserRole;
  username?: string;
  avatar?: string;
  isMaster?: boolean;
}

export interface IAuthSlice {
  token?: string;
  user?: IUser;
  loginStatus: ERequestStatus;
  registerStatus: ERequestStatus;
}

const initialState: IAuthSlice = {
  loginStatus: ERequestStatus.IDLE,
  registerStatus: ERequestStatus.IDLE,
  ...getCredentialsFromCookie(),
};

export const login = createAsyncThunk('auth/login', AuthAPI.login);

export const register = createAsyncThunk(
  'auth/register',
  async ({
    user,
    invitationUUID,
  }: {
    user: CreateUserDtoClient;
    invitationUUID: string;
  }) => {
    const response = await AuthAPI.register(user, invitationUUID);
    return response.data;
  },
);

/**
 * Login handler.
 * @param state - auth slice state
 */
const handleLogin = (state: IAuthSlice) => {
  const { user, token } = getCredentialsFromCookie();

  if (user) {
    state.loginStatus = ERequestStatus.SUCCESS;
    state.user = user;
    state.token = token;
  } else {
    state.loginStatus = ERequestStatus.FAILED;
  }
};

/**
 * Auth slice.
 */
export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: (state) => {
      state.user = undefined;
      state.token = undefined;
      state.loginStatus = ERequestStatus.IDLE;

      Cookie.remove('token');
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.rejected, (state) => {
        state.loginStatus = ERequestStatus.FAILED;
      })
      .addCase(login.pending, (state) => {
        state.loginStatus = ERequestStatus.LOADING;
      })
      .addCase(login.fulfilled, handleLogin);
    builder
      .addCase(register.rejected, (state) => {
        state.registerStatus = ERequestStatus.FAILED;
      })
      .addCase(register.pending, (state) => {
        state.registerStatus = ERequestStatus.LOADING;
      })
      .addCase(register.fulfilled, (state) => {
        state.registerStatus = ERequestStatus.SUCCESS;
        handleLogin(state);
      });
  },
});

export const selectUser = (state: TAppState) => state.auth.user;
export const selectLoginStatus = (state: TAppState) => state.auth.loginStatus;
export const selectRegisterStatus = (state: TAppState) =>
  state.auth.registerStatus;

export const { logout } = authSlice.actions;
