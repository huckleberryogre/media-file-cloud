import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from '@reduxjs/toolkit';
import { FilesAPI } from 'api';
import { ERequestStatus } from 'enums';
import { IAsyncData, IDirectory, IFileEntry } from 'models';
import { TAppState } from 'store';
import { set as _set, uniq as _uniq } from 'lodash';
import { login, logout } from '../auth';

interface IFilesSlice {
  entries: IAsyncData<IFileEntry[]>;
  filesUrls: Record<string, string>;
  directories: IAsyncData<IDirectory[]>;
  shared: {
    fileEntries: IAsyncData<IFileEntry[]>;
  };
}

const initialState: IFilesSlice = {
  entries: {
    data: [],
    status: ERequestStatus.IDLE,
  },
  filesUrls: {},
  directories: {
    data: [],
    status: ERequestStatus.IDLE,
  },
  shared: {
    fileEntries: {
      data: [],
      status: ERequestStatus.IDLE,
    },
  },
};

export const getAllFilesEntries = createAsyncThunk(
  'files/getAllFilesEntries',
  async () => {
    const response = await FilesAPI.getAllFilesEntries();

    return response.data;
  },
);

export const getSharedFileEntry = createAsyncThunk(
  'files/getSharedFileEntry',
  async (uuid: string) => {
    const response = await FilesAPI.getSharedFilesEntries(uuid);

    return response.data;
  },
);

export const getAllDirectories = createAsyncThunk(
  'files/getAllDirectories',
  async () => {
    const response = await FilesAPI.getAllDirectoryEntries();

    return response.data;
  },
);

export const getFileUrl = createAsyncThunk(
  'files/getFile',
  async (fileEntry: IFileEntry, thunkAPI) => {
    const state = thunkAPI.getState() as TAppState;
    const { uuid } = fileEntry;

    if (state.files.filesUrls[uuid]) {
      return state.files.filesUrls[uuid];
    } else {
      const file = await FilesAPI.getFile(fileEntry);

      return URL.createObjectURL(file);
    }
  },
);

export const filesSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllFilesEntries.rejected, (state) => {
        state.entries.status = ERequestStatus.FAILED;
      })
      .addCase(getAllFilesEntries.pending, (state) => {
        state.entries.status = ERequestStatus.LOADING;
      })
      .addCase(getAllFilesEntries.fulfilled, (state, action) => {
        state.entries.status = ERequestStatus.SUCCESS;
        state.entries.data = action.payload;
      })
      .addCase(getAllDirectories.rejected, (state) => {
        state.directories.status = ERequestStatus.FAILED;
      })
      .addCase(getAllDirectories.pending, (state) => {
        state.directories.status = ERequestStatus.LOADING;
      })
      .addCase(getAllDirectories.fulfilled, (state, action) => {
        state.directories.status = ERequestStatus.SUCCESS;
        state.directories.data = action.payload;
      })
      .addCase(getFileUrl.fulfilled, (state, action) => {
        state.filesUrls[action.meta.arg.uuid] = action.payload;
      })
      .addCase(logout, () => initialState)
      .addCase(login.fulfilled, () => initialState)
      .addCase(getSharedFileEntry.rejected, (state) => {
        state.shared.fileEntries.status = ERequestStatus.FAILED;
      })
      .addCase(getSharedFileEntry.pending, (state) => {
        state.shared.fileEntries.status = ERequestStatus.LOADING;
      })
      .addCase(getSharedFileEntry.fulfilled, (state, action) => {
        state.shared.fileEntries.status = ERequestStatus.SUCCESS;
        state.shared.fileEntries.data = action.payload;
      });
  },
});

export const selectAllFilesEntries = (state: TAppState) =>
  state.files.entries.data;
export const selectAllFilesEntriesStatus = (state: TAppState) =>
  state.files.entries.status;
export const selectAllDirectories = (state: TAppState) =>
  state.files.directories.data;
export const selectAllDirectoriesStatus = (state: TAppState) =>
  state.files.directories.status;
export const selectSharedFileEntry = (state: TAppState) =>
  state.files.shared.fileEntries.data;
export const selectSharedFileEntryStatus = (state: TAppState) =>
  state.files.shared.fileEntries.status;

export const selectFoldersMap = createSelector(
  [(state: TAppState) => state.files.entries.data],
  (files) =>
    files.reduce((map, file) => {
      if (file.path.length) {
        _set(map, file.path, {});
      }
      return map;
    }, {}),
);

export const selectCurrentLevelSubdirectories = (
  currentLevelDirectory: string[],
) =>
  createSelector([selectAllDirectories], (directories) => {
    const directoriesArray = directories.map((directory) => directory.path);

    const currentLevelSubdirectories = directoriesArray.reduce(
      (result: string[], directory: string) => {
        const directorySplit = _uniq(directory.split(/\/|\\/).filter(Boolean));

        if (
          currentLevelDirectory.every(
            (dir, index) => dir === directorySplit[index],
          ) &&
          currentLevelDirectory.length === directorySplit.length - 1
        ) {
          result.push(directorySplit[currentLevelDirectory.length]);
        }

        return result;
      },
      [],
    );

    return currentLevelSubdirectories;
  });
