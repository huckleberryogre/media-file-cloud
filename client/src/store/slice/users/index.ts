import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { UsersAPI } from 'api/usersAPI';
import { ERequestStatus } from 'enums';
import { TAppState } from 'store';

export const getFeatures = createAsyncThunk('users/features', async () => {
  const response = await UsersAPI.getFeatures();
  return { data: response.data };
});

const initialState = {
  requestStatus: ERequestStatus.IDLE,
  features: {
    googleAuth: false,
    hasMasterUser: true,
  },
};

export const featuresSlice = createSlice({
  name: 'features',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getFeatures.fulfilled, (state, { payload }) => {
        state.features = payload.data;
        state.requestStatus = ERequestStatus.SUCCESS;
      })
      .addCase(getFeatures.pending, (state) => {
        state.requestStatus = ERequestStatus.LOADING;
      })
      .addCase(getFeatures.rejected, (state) => {
        state.requestStatus = ERequestStatus.FAILED;
      });
  },
});

export const selectFeatures = (state: TAppState) => state.features;
