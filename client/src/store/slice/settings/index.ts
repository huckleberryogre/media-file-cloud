import { createSlice } from '@reduxjs/toolkit';
import { EAppTheme } from 'enums';
import { TAppState } from '../../store';
import { login, logout } from '../auth';

export interface ISettingsSlice {
  mode: EAppTheme;
  isPlayerBarExpanded: boolean;
  isDrawerOpen: boolean;
}

const initialState: ISettingsSlice = {
  mode: EAppTheme.DARK,
  isPlayerBarExpanded: false,
  isDrawerOpen: false,
};

/**
 * Settings slice.
 */
export const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    setMode: (state, action) => {
      state.mode = action.payload;
    },
    setIsPlayerBarExpanded: (state, action) => {
      state.isPlayerBarExpanded = action.payload;
    },
    setIsDrawerOpen: (state, action) => {
      state.isDrawerOpen = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(logout, () => initialState)
      .addCase(login.fulfilled, () => initialState);
  },
});

export const { setMode, setIsPlayerBarExpanded, setIsDrawerOpen } =
  settingsSlice.actions;
export const selectMode = (state: TAppState) => state.settings.mode;
export const selectIsPlayerBarExpanded = (state: TAppState) =>
  state.settings.isPlayerBarExpanded;
export const selectIsDrawerOpen = (state: TAppState) =>
  state.settings.isDrawerOpen;
