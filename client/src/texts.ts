/**
 * Validation messages.
 */
export const VALIDATION_MESSAGES = {
  required: 'This field is required',
  minLength: (minLength?: number) =>
    `This field is too short${
      minLength ? ` (min ${minLength} characters)` : ''
    }`,
  passwordsMatch: 'Passwords do not match',
};
