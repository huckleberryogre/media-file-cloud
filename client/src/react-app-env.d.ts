/// <reference types="react-scripts" />

//https://react-swipeable-views.com/
declare module 'react-swipeable-views';
declare module '*.lottie' {
  const src: string;
  export default src;
}
