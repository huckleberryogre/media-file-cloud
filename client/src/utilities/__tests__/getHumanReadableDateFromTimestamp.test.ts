import { getHumanReadableDateFromTimestamp } from '../getHumanReadableDateFromTimestamp';

describe('getHumanReadableDateFromTimestamp', () => {
  it('should return empty string for non-number values', () => {
    expect(getHumanReadableDateFromTimestamp(null as any)).toBe('');
    expect(getHumanReadableDateFromTimestamp(false as any)).toBe('');
    expect(getHumanReadableDateFromTimestamp(NaN as any)).toBe('');
    expect(getHumanReadableDateFromTimestamp(undefined as any)).toBe('');
    expect(getHumanReadableDateFromTimestamp({} as any)).toBe('');
    expect(getHumanReadableDateFromTimestamp('1' as any)).toBe('');
    expect(getHumanReadableDateFromTimestamp([1] as any)).toBe('');
    expect(getHumanReadableDateFromTimestamp((() => {}) as any)).toBe('');
  });

  it('should return expected date formats', () => {
    expect(getHumanReadableDateFromTimestamp(1629478437765)).toBe('20/08/2021');
    expect(getHumanReadableDateFromTimestamp(1577871039000)).toBe('01/01/2020');
    expect(getHumanReadableDateFromTimestamp(978217200000)).toBe('31/12/2000');
  });
});
