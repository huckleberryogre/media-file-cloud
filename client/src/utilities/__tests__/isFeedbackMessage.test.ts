import { isFeedback } from '../isFeedback';

describe('isFeedback', () => {
  it('should return false', () => {
    expect(isFeedback()).toBe(false);
    expect(isFeedback({})).toBe(false);
    expect(isFeedback([])).toBe(false);
    expect(isFeedback(1)).toBe(false);
    expect(isFeedback('1')).toBe(false);
    expect(isFeedback('')).toBe(false);
    expect(isFeedback(true)).toBe(false);
    expect(isFeedback(false)).toBe(false);
    expect(isFeedback({ info: 'info', test: '123' })).toBe(false);
  });

  it('should return true', () => {
    expect(isFeedback({ info: 'info' })).toBe(true);
    expect(isFeedback({ info: ['info'] })).toBe(true);
    expect(
      isFeedback({
        info: 'info',
        error: 'error',
        warning: ['warning1', 'warning2'],
        success: 'success',
      }),
    ).toBe(true);
  });
});
