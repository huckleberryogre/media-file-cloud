import Cookie from 'js-cookie';
import { jwtDecode } from 'jwt-decode';
import { IUser } from '../store/slice/auth';

/**
 * Returns credentials from the token cookie (if set).
 */
export const getCredentialsFromCookie = () => {
  let user, token;

  try {
    token = Cookie.get('token');

    if (token) {
      user = jwtDecode<IUser>(token);
    }
  } catch (e) {
    console.error('Failed to login: ', e);
  }

  return { user, token };
};
