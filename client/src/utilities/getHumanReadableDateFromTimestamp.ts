const isNumber = (value: any): value is number => {
  return typeof value === 'number' && isFinite(value);
};

/**
 * Returns human readable date format from the given timestamp.
 */
export const getHumanReadableDateFromTimestamp = (timestamp: number) => {
  return isNumber(timestamp)
    ? new Date(timestamp).toLocaleDateString('en-GB')
    : '';
};
