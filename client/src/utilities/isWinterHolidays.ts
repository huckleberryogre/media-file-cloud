/**
 * Checks if it is winter holidays.
 */
export const isWinterHolidays = () => {
  const now = new Date();

  return now.getMonth() === 11 || now.getMonth() === 0;
};
