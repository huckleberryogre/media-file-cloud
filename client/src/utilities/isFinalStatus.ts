import { ERequestStatus } from '../enums';

/**
 * Returns true if the request status is final.
 * @param status Request status.
 */
export const isFinalStatus = (status: ERequestStatus) => {
  return status === ERequestStatus.SUCCESS || status === ERequestStatus.FAILED;
};
