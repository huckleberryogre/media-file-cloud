import { IFeedback } from '@shared-models';
import { difference, isEmpty } from 'lodash';

export const isFeedback = (arg?: any): arg is IFeedback => {
  // not an object
  if (typeof arg !== 'object') {
    return false;
  }
  // keys within the right preset ['warning', 'info', 'error', 'success']
  if (
    !isEmpty(
      difference(Object.keys(arg), ['warning', 'info', 'error', 'success']),
    )
  ) {
    return false;
  }

  const { error, info, warning, success } = arg as IFeedback;

  // at least one severity type is present
  return (
    !isEmpty(error) || !isEmpty(info) || !isEmpty(warning) || !isEmpty(success)
  );
};
