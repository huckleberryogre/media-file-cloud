import React, { useEffect } from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { BrowserRouter } from 'react-router-dom';
import { useAppDispatch } from './store';
import { getAllDirectories, getAllFilesEntries } from './store/slice/files';
import { useSnackbar } from 'notistack';
import { last } from 'lodash';
import { useSelector } from 'react-redux';
import {
  notificationActions,
  notificationsSelector,
} from './store/slice/notifications';
import { MainLayout } from './components/Layout/MainLayout';
import { selectUser } from './store/slice/auth';
import { NotistackActionButton } from './components/Notistack/Notistack';
import { selectMode } from './store/slice/settings';
import { IFeedback } from '@shared-models';
import { showFeedback } from './api/interceptors';
import { PlayerBar } from './components/PlayerBar/PlayerBar';
import { Router } from './components/Router/Router';

export const App = () => {
  const dispatch = useAppDispatch();
  const user = useSelector(selectUser);
  const notifications = useSelector(notificationsSelector);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const mode = useSelector(selectMode);
  const eventSourceRef = React.useRef<EventSource | null>(null);

  const theme = React.useMemo(() => createTheme({ palette: { mode } }), [mode]);

  useEffect(() => {
    if (eventSourceRef.current) {
      eventSourceRef.current.close();
    }

    if (user) {
      // initial request
      dispatch(getAllFilesEntries());
      dispatch(getAllDirectories());

      // subscribe to changes
      eventSourceRef.current = new EventSource(
        `${process.env.REACT_APP_SERVER_URL}/api/files/files-changed`,
      );

      eventSourceRef.current.onmessage = ({ data }) => {
        try {
          const payload = JSON.parse(data);

          dispatch({
            type: getAllFilesEntries.fulfilled.type,
            payload,
          });
        } catch (error) {
          console.error(error);
          dispatch(
            notificationActions.show({
              message: 'Error fetching files',
              severity: 'error',
              duration: 1500,
            }),
          );
        }
      };
      eventSourceRef.current.onerror = (error) => {
        console.error('EventSource for api/files/files-changed failed:', error);
      };
    }
  }, [dispatch, user]);

  useEffect(() => {
    const eventSource = new EventSource(
      `${process.env.REACT_APP_SERVER_URL}/api/notifications/feedback`,
    );

    eventSource.onmessage = ({ data }) => {
      const feedback = JSON.parse(data) as IFeedback;
      showFeedback(feedback);
    };
    eventSource.onerror = (error) => {
      console.error(
        'EventSource for api/notifications/feedback failed:',
        error,
      );
    };
  }, []);

  useEffect(() => {
    const notification = last(notifications);
    if (notification) {
      const { uuid, message, severity, duration } = notification;

      enqueueSnackbar(message, {
        autoHideDuration: duration,
        key: uuid,
        variant: severity,
        action: <NotistackActionButton onClick={() => closeSnackbar(uuid)} />,
      });
    }
  }, [closeSnackbar, enqueueSnackbar, notifications, notifications.length]);

  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <MainLayout>
          <Router />
          <PlayerBar />
        </MainLayout>
      </BrowserRouter>
    </ThemeProvider>
  );
};
