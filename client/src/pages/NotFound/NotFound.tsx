import { Typography, Box } from '@mui/material';

export const NotFound = () => {
  return (
    <Box m="auto">
      <Typography fontSize="12rem" color="secondary">
        404
      </Typography>
    </Box>
  );
};
