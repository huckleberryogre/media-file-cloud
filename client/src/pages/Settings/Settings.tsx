import { Box, Switch, Typography } from '@mui/material';
import { AuthAPI, FilesAPI } from 'api';
import { notificationActions } from 'store/slice/notifications';
import {
  LinkRounded as LinkIcon,
  RefreshRounded as RefreshIcon,
} from '@mui/icons-material';
import { useAppDispatch } from 'store';
import { selectUser } from 'store/slice/auth';
import { useSelector } from 'react-redux';
import { selectMode, setMode } from 'store/slice/settings';
import { EAppTheme } from 'enums';
import { useState } from 'react';
import LoadingButton from '@mui/lab/LoadingButton';

export const Settings = () => {
  const dispatch = useAppDispatch();
  const [inviteLink, setInviteLink] = useState<string>('');
  const [
    isLoadingRecalculateFilesMetadata,
    setIsLoadingRecalculateFilesMetadata,
  ] = useState(false);
  const [isLoadingCreateInviteLink, setIsLoadingCreateInviteLink] =
    useState(false);
  // set theme
  const mode = useSelector(selectMode);
  const user = useSelector(selectUser);

  const copyInviteLinkToClipboard = async (link: string) => {
    try {
      await navigator.clipboard.writeText(link);
      dispatch(
        notificationActions.show({
          message: 'Invite link copied',
          severity: 'info',
        }),
      );
    } catch (error) {
      console.error('Failed to copy invite link to clipboard', error);
      dispatch(
        notificationActions.show({
          message: 'Failed to copy invite link to clipboard',
          severity: 'warning',
        }),
      );
    }
  };

  const handleClickCreateInviteLink = async () => {
    setIsLoadingCreateInviteLink(true);
    await AuthAPI.createRegistrationInvite().then((res) => {
      if (res.data.uuid) {
        const link = `${window.location.origin}/register/${res.data.uuid}`;
        setInviteLink(link);
        copyInviteLinkToClipboard(link);
      } else {
        dispatch(
          notificationActions.show({
            message: 'Failed to create invite link',
            severity: 'error',
          }),
        );
      }
    });
    setIsLoadingCreateInviteLink(false);
  };

  const handleClickChangeMode = () => {
    dispatch(
      setMode(mode === EAppTheme.DARK ? EAppTheme.LIGHT : EAppTheme.DARK),
    );
  };

  const handleClickRecalculateFilesMetadata = async () => {
    setIsLoadingRecalculateFilesMetadata(true);
    await FilesAPI.recalculateAllMetadata();
    setIsLoadingRecalculateFilesMetadata(false);
  };

  return (
    <Box display="block" width="100%">
      <Typography variant="h4">Settings</Typography>
      <hr />
      {user?.isMaster && (
        <Box display="flex" my={2}>
          <Typography display="inline" mr={1} variant="h6" alignSelf="center">
            Invite your friend:
          </Typography>
          <LoadingButton
            loading={isLoadingCreateInviteLink}
            onClick={handleClickCreateInviteLink}
            startIcon={<LinkIcon />}
            variant="contained"
            color="primary"
            aria-label="create invite link"
            size="small"
          >
            Create invite link
          </LoadingButton>
          <Typography display="inline" ml={1} variant="h6" alignSelf="center">
            {inviteLink && (
              <>
                | {inviteLink} | &nbsp;
                <LoadingButton
                  onClick={() => copyInviteLinkToClipboard(inviteLink)}
                  startIcon={<LinkIcon />}
                  variant="contained"
                  color="primary"
                  aria-label="copy invite link"
                  size="small"
                >
                  Copy to clipboard
                </LoadingButton>
              </>
            )}
          </Typography>
        </Box>
      )}
      <Box display="flex" my={2} alignItems="center">
        <Typography variant="h6" mr={1}>
          Dark mode:
        </Typography>
        <Typography>Off</Typography>
        <Switch
          checked={mode === EAppTheme.DARK}
          onChange={handleClickChangeMode}
        />
        <Typography>On</Typography>
      </Box>
      <Box display="flex" alignItems="center">
        <Typography display="inline" mr={1} variant="h6" alignSelf="center">
          Recalculate files metadata:
        </Typography>
        <LoadingButton
          loading={isLoadingRecalculateFilesMetadata}
          onClick={handleClickRecalculateFilesMetadata}
          startIcon={<RefreshIcon />}
          variant="contained"
          color="primary"
          aria-label="recalculate files metadata"
          size="small"
        >
          Recalculate
        </LoadingButton>
      </Box>
    </Box>
  );
};
