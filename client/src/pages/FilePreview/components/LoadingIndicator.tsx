import { Box } from '@mui/material';

interface IProps {
  show: boolean;
}

export const LoadingIndicator = ({ show }: IProps) => {
  return show ? (
    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
      <h1>Loading...</h1>
    </Box>
  ) : null;
};
