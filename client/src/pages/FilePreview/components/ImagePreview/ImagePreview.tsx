import { Box, Divider, Theme, Typography, useMediaQuery } from '@mui/material';
import * as React from 'react';
import { IFileEntry } from '../../../../models';
import { TagsAutocomplete } from '../../../../components/TagsAutocomplete/TagsAutocomplete';

interface IProps {
  fileEntry: IFileEntry;
}

export const ImagePreview = ({ fileEntry }: IProps) => {
  const isLargeScreen = useMediaQuery<Theme>((theme) =>
    theme.breakpoints.up('md'),
  );

  return (
    <Box
      display="flex"
      flexWrap="wrap"
      justifyContent="space-evenly"
      alignItems="flex-end"
      gap={2}
    >
      <Typography variant="h5" component="h1">
        {fileEntry.name}
      </Typography>
      {isLargeScreen && <Divider orientation="vertical" flexItem />}
      <Box
        display="inline-block"
        sx={{
          minWidth: '300px',
          flexGrow: 1,
        }}
      >
        <TagsAutocomplete fileEntry={fileEntry} />
      </Box>
      <Box
        component="img"
        maxWidth="100%"
        src={`/api/files/${fileEntry.uuid}`}
        alt={fileEntry.name}
      />
    </Box>
  );
};
