import { IFileEntry } from 'models';
import {
  Box,
  Divider,
  IconButton,
  Theme,
  Typography,
  useMediaQuery,
} from '@mui/material';
import { TagsAutocomplete } from 'components/TagsAutocomplete/TagsAutocomplete';
import { TAudioMetadataClientModel } from '@shared-models';
import { PlayArrowRounded as PlayArrowIcon } from '@mui/icons-material';
import { useAppDispatch } from 'store';
import {
  playFromStart,
  setCurrentTrackFileEntry,
  setIsPlaying,
} from 'store/slice/player';
import { setIsPlayerBarExpanded } from 'store/slice/settings';

interface IProps {
  fileEntry: IFileEntry;
}

export const AudioPreview = ({ fileEntry }: IProps) => {
  const dispatch = useAppDispatch();
  const metadata = fileEntry.metadata as TAudioMetadataClientModel;
  const isLargeScreen = useMediaQuery<Theme>((theme) =>
    theme.breakpoints.up('md'),
  );
  const divider = isLargeScreen ? (
    <Divider orientation="vertical" flexItem />
  ) : null;

  const handleClickPlay = () => {
    dispatch(setCurrentTrackFileEntry(fileEntry));
    dispatch(setIsPlaying(true));
    dispatch(playFromStart());
    dispatch(setIsPlayerBarExpanded(true));
  };

  return (
    <>
      <Box
        display="flex"
        flexWrap="wrap"
        justifyContent="space-evenly"
        alignItems="flex-end"
        gap={2}
      >
        <IconButton aria-label="play" onClick={handleClickPlay}>
          <PlayArrowIcon />
        </IconButton>
        {divider}
        <Typography variant="h5" component="h1">
          {metadata.common.track.no && `${metadata.common.track.no}.`}
          {metadata.common.title || fileEntry.name}
        </Typography>
        {divider}
        <Typography variant="subtitle1" component="h2">
          {metadata.common.albumartist} - {metadata.common.album}
          {metadata.common.year && ` (${metadata.common.year})`}
        </Typography>
        {divider}
        <Box
          display="inline-block"
          sx={{
            minWidth: '300px',
            flexGrow: 1,
          }}
        >
          <TagsAutocomplete fileEntry={fileEntry} />
        </Box>
      </Box>
    </>
  );
};
