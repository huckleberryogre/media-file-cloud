import { Typography } from '@mui/material';
import { TagsAutocomplete } from 'components/TagsAutocomplete/TagsAutocomplete';
import { IFileEntry } from 'models';

interface IProps {
  fileEntry: IFileEntry;
}

export const DefaultPreview = ({ fileEntry }: IProps) => {
  return (
    <>
      <Typography mb={2} variant="h4" component="h1">
        {fileEntry.name}
      </Typography>
      <TagsAutocomplete fileEntry={fileEntry} />
    </>
  );
};
