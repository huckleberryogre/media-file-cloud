import { IFileEntry } from 'models';
import { Box, Divider, Theme, Typography, useMediaQuery } from '@mui/material';
import { TagsAutocomplete } from 'components/TagsAutocomplete/TagsAutocomplete';
import { getFileUrl } from 'store/slice/files';
import { useAppDispatch } from 'store';
import { useEffect, useRef, useState } from 'react';
import { ErrorBoundary } from 'components/ErrorBoundary/ErrorBoundary';
import videojs from 'video.js';
import 'video.js/dist/video-js.css';

interface IProps {
  fileEntry: IFileEntry;
}

export const VideoPreview = ({ fileEntry }: IProps) => {
  const dispatch = useAppDispatch();
  const isLargeScreen = useMediaQuery<Theme>((theme) =>
    theme.breakpoints.up('md'),
  );
  const [fileUrl, setFileUrl] = useState<string>();
  const videoReference = useRef<HTMLVideoElement>(null);

  useEffect(() => {
    dispatch(getFileUrl(fileEntry)).then((data) => {
      setFileUrl(data.payload as string);
    });
  }, [dispatch, fileEntry]);

  useEffect(() => {
    try {
      if (videoReference.current && fileUrl) {
        const player = videojs(videoReference.current, {
          controls: true,
          sources: [
            {
              src: fileUrl,
              type: fileEntry.mimeType,
            },
          ],
          muted: true,
          height: Math.max(window.innerHeight - 200, 200),
          width: Math.max(window.innerWidth - 400, 350),
        });
      }
    } catch (error) {
      console.error(error);
    }

    return () => {
      videojs.getAllPlayers().forEach((player) => player.dispose());
    };
  }, [fileEntry.mimeType, fileUrl, videoReference.current]);

  console.log(fileUrl);
  return (
    <>
      <Box
        display="flex"
        flexWrap="wrap"
        justifyContent="space-evenly"
        alignItems="flex-end"
        gap={2}
      >
        <Typography variant="h5" component="h1">
          {fileEntry.name}
        </Typography>
        {isLargeScreen && <Divider orientation="vertical" flexItem />}
        <Box
          display="inline-block"
          sx={{
            minWidth: '300px',
            flexGrow: 1,
          }}
        >
          <TagsAutocomplete fileEntry={fileEntry} />
        </Box>
        <Box width="100%">
          <ErrorBoundary>
            <video ref={videoReference} className="video-js" />
          </ErrorBoundary>
        </Box>
      </Box>
    </>
  );
};
