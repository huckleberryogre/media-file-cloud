import { useSelector } from 'react-redux';
import {
  getSharedFileEntry,
  selectAllFilesEntries,
  selectAllFilesEntriesStatus,
  selectSharedFileEntry,
  selectSharedFileEntryStatus,
} from 'store/slice/files';
import { getAllTags, selectTagsStatus } from 'store/slice/tags';
import { useAppDispatch } from 'store';
import { useParams } from 'react-router-dom';
import { useEffect } from 'react';
import { ERequestStatus } from 'enums';
import { Box } from '@mui/material';
import { ImagePreview } from './components/ImagePreview/ImagePreview';
import { AudioPreview } from './components/AudioPreview/AudioPreview';
import { DefaultPreview } from './components/DefaultPreview/DefaultPreview';
import { LoadingIndicator } from './components/LoadingIndicator';
import { VideoPreview } from './components/VideoPreview/VideoPreview';
import { setIsPlayerBarExpanded } from 'store/slice/settings';

interface IProps {
  shouldLoadSharedFileEntity?: boolean;
}

export const FilePreview = ({ shouldLoadSharedFileEntity }: IProps) => {
  const sharedFileEntry = useSelector(selectSharedFileEntry);
  const sharedFileEntryRequestStatus = useSelector(selectSharedFileEntryStatus);
  const fileEntries = useSelector(selectAllFilesEntries);
  const filesEntriesRequestStatus = useSelector(selectAllFilesEntriesStatus);
  const tagsRequestStatus = useSelector(selectTagsStatus);
  const dispatch = useAppDispatch();
  const { uuid: fileUUID } = useParams<{ uuid: string }>();
  const fileEntry = [...fileEntries, ...sharedFileEntry].find(
    (file) => file.uuid === fileUUID,
  );

  useEffect(() => {
    if (tagsRequestStatus === ERequestStatus.IDLE) {
      dispatch(getAllTags());
    }
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
  }, []);

  useEffect(() => {
    if (!fileEntry?.mimeType.startsWith('audio')) {
      dispatch(setIsPlayerBarExpanded(false));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fileUUID, dispatch]);

  useEffect(() => {
    if (shouldLoadSharedFileEntity && fileUUID) {
      dispatch(getSharedFileEntry(fileUUID));
    }
  }, [dispatch, fileUUID, shouldLoadSharedFileEntity]);

  let preview = null;
  const fileType = fileEntry?.mimeType.split('/')[0];

  if (!!fileEntry) {
    switch (fileType) {
      case 'image':
        preview = <ImagePreview fileEntry={fileEntry} />;
        break;
      case 'audio':
        preview = <AudioPreview fileEntry={fileEntry} />;
        break;
      case 'video':
        preview = <VideoPreview fileEntry={fileEntry} />;
        break;
      default:
        preview = <DefaultPreview fileEntry={fileEntry} />;
    }
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <LoadingIndicator
        show={
          filesEntriesRequestStatus === ERequestStatus.LOADING ||
          sharedFileEntryRequestStatus === ERequestStatus.LOADING
        }
      />
      {preview}
    </Box>
  );
};
