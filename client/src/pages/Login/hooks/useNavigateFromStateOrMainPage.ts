import { useLocation, useNavigate } from 'react-router-dom';
import { useCallback } from 'react';

export const useNavigateFromStateOrMainPage = () => {
  const location = useLocation();
  const navigate = useNavigate();

  const from = location.state?.from || '/';

  const navigateBack = useCallback(() => {
    if (from) {
      navigate(from);
    } else {
      navigate('/');
    }
  }, [from, navigate]);

  return { navigateBack };
};
