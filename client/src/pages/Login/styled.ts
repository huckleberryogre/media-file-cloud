import { styled } from '@mui/material';
import { TabPanel } from '../../components/TabPanel/TabPanel';
import { Alert } from '../../components/Alert/Alert';

export const TabPanelStyled = styled(TabPanel)(({ theme }) => ({
  minHeight: 220,
  padding: theme.spacing(2),
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
}));

export const TabAlertStyled = styled(Alert)(({ theme }) => ({
  textAlign: 'center',
  margin: theme.spacing(0, -2, 1.5),
  justifyContent: 'center',
  borderRadius: 0,
  color: theme.palette.text.primary,
}));
