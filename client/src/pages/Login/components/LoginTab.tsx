import { useSelector } from 'react-redux';
import { login, selectLoginStatus } from '../../../store/slice/auth';
import { Box, Divider, TextField, Tooltip } from '@mui/material';
import GoogleIcon from '@mui/icons-material/Google';
import { TabPanelStyled } from '../styled';
import React, { useEffect } from 'react';
import { ETab } from '../Login';
import { useAppDispatch } from '../../../store';
import { ERequestStatus } from '../../../enums';
import LoadingButton from '@mui/lab/LoadingButton';
import { getPopupWindowCentralPosition } from '../../../utilities/getPopupWindowCentralPosition';
import { getFeatures, selectFeatures } from '../../../store/slice/users';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useNavigateFromStateOrMainPage } from '../hooks/useNavigateFromStateOrMainPage';
import { TabAlert } from './TabAlert';
import { TEXTS } from './TEXTS';

interface IProps {
  activeTabIndex: ETab;
}

interface IFormValues {
  email: string;
  password: string;
}

export const LoginTab = ({ activeTabIndex }: IProps) => {
  const dispatch = useAppDispatch();
  const { navigateBack } = useNavigateFromStateOrMainPage();
  const status = useSelector(selectLoginStatus);
  const { requestStatus: featuresRequestStatus, features } =
    useSelector(selectFeatures);
  const { register, handleSubmit } = useForm<IFormValues>();

  useEffect(() => {
    dispatch(getFeatures());
  }, [dispatch]);

  const handleClickLogin: SubmitHandler<IFormValues> = async ({
    email,
    password,
  }) => {
    await dispatch(login({ email, password }));

    navigateBack();
  };

  /**
   * Google login in development mode only works when the client
   * is served from the same domain as the server.
   */
  const handleDevGoogleAuth = () => {
    if (
      process.env.REACT_APP_ENVIRONMENT === 'development' &&
      window.location.href.includes(process.env.REACT_APP_CLIENT_URL as string)
    ) {
      window.open(process.env.REACT_APP_SERVER_URL, '_self');
    }
  };

  const handleClickGoogle = () => {
    handleDevGoogleAuth();

    const { top, left } = getPopupWindowCentralPosition(450, 600);

    window.open(
      `${process.env.REACT_APP_SERVER_URL}/api/auth/google`,
      '_blank',
      `popup,height=600,width=450,top=${top},left=${left}`,
    );
  };

  const hasGoogleAuth = features?.googleAuth;

  return (
    <TabPanelStyled activeTabIndex={activeTabIndex} index={ETab.LOGIN}>
      <TabAlert text={TEXTS.noMasterUser} show={!features.hasMasterUser} />
      <Box component="form" textAlign="center">
        <TextField
          type="email"
          label="email"
          size="small"
          fullWidth
          sx={{ mb: 1.5 }}
          autoComplete="email"
          {...register('email', { required: true })}
        />
        <TextField
          type="password"
          label="password"
          size="small"
          fullWidth
          sx={{ mb: 1 }}
          autoComplete="current-password"
          {...register('password', { required: true })}
        />
        <LoadingButton
          loading={status === ERequestStatus.LOADING}
          variant="text"
          onClick={handleSubmit(handleClickLogin)}
          sx={{ mb: 1 }}
          type="submit"
        >
          login
        </LoadingButton>
      </Box>
      <Divider sx={{ ml: -2, mr: -2, mb: 1 }} />
      <Tooltip title={!hasGoogleAuth ? 'Google auth is not enabled' : ''}>
        <Box textAlign="center">
          <LoadingButton
            fullWidth
            color="secondary"
            onClick={handleClickGoogle}
            startIcon={<GoogleIcon />}
            disabled={!hasGoogleAuth}
            loading={featuresRequestStatus === ERequestStatus.LOADING}
          >
            continue with google
          </LoadingButton>
        </Box>
      </Tooltip>
    </TabPanelStyled>
  );
};
