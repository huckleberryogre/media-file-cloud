export const TEXTS = {
  noMasterUser: 'Register now to create a master user',
  noRegistrationInvite: 'Ask administrator for an invite link',
};
