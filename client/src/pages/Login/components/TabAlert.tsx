import { WarningRounded as WarningIcon } from '@mui/icons-material';
import { Divider, Typography } from '@mui/material';
import { TabAlertStyled } from '../styled';
import { AlertColor } from '@mui/material/Alert/Alert';

interface IProps {
  show: boolean;
  text: string;
  severity?: AlertColor;
}

export const TabAlert = ({ show, text, severity = 'info' }: IProps) => {
  return show ? (
    <>
      <TabAlertStyled
        severity={severity}
        variant={'filled'}
        icon={<WarningIcon />}
      >
        <Typography variant={'subtitle1'}>{text}</Typography>
      </TabAlertStyled>
      <Divider sx={{ ml: -2, mr: -2, mb: 2 }} />
    </>
  ) : null;
};
