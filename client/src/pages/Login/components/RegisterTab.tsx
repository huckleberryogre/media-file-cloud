import { Box, TextField } from '@mui/material';
import { TabPanelStyled } from '../styled';
import { ETab } from '../Login';
import { ERequestStatus } from '../../../enums';
import { useSelector } from 'react-redux';
import {
  register as registerAction,
  selectRegisterStatus,
} from '../../../store/slice/auth';
import { useAppDispatch } from '../../../store';
import { LoadingButton } from '@mui/lab';
import { SubmitHandler, useForm } from 'react-hook-form';
import { VALIDATION_MESSAGES } from '../../../texts';
import { TabAlert } from './TabAlert';
import React from 'react';
import { selectFeatures } from '../../../store/slice/users';
import { useParams } from 'react-router-dom';
import { TEXTS } from './TEXTS';
import { notificationActions } from '../../../store/slice/notifications';
import { useNavigateFromStateOrMainPage } from '../hooks/useNavigateFromStateOrMainPage';

interface IProps {
  activeTabIndex: ETab;
}

interface IFormValues {
  email: string;
  password: string;
  passwordConfirm: string;
}

export const RegisterTab = ({ activeTabIndex }: IProps) => {
  const dispatch = useAppDispatch();
  const { navigateBack } = useNavigateFromStateOrMainPage();
  const { uuid } = useParams();
  const status = useSelector(selectRegisterStatus);
  const { features } = useSelector(selectFeatures);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormValues>();

  const handleClickRegister: SubmitHandler<IFormValues> = async ({
    email,
    password,
  }) => {
    if (!uuid) {
      dispatch(
        notificationActions.show({
          message: 'Registration invite is required',
          severity: 'error',
        }),
      );
    } else {
      await dispatch(
        registerAction({ user: { email, password }, invitationUUID: uuid }),
      );

      navigateBack();
    }
  };

  const isLoading = status === ERequestStatus.LOADING;

  const disabled = !features.hasMasterUser || !uuid || isLoading;

  return (
    <TabPanelStyled activeTabIndex={activeTabIndex} index={ETab.REGISTER}>
      <TabAlert
        text={
          !features.hasMasterUser
            ? TEXTS.noMasterUser
            : TEXTS.noRegistrationInvite
        }
        show={!features.hasMasterUser || !uuid}
        severity={!features.hasMasterUser ? 'info' : 'warning'}
      />
      <Box component="form" textAlign="center">
        <TextField
          disabled={disabled}
          error={!!errors.email}
          helperText={errors.email?.message}
          type="email"
          label="email"
          size="small"
          fullWidth
          autoComplete="email"
          {...register('email', {
            required: VALIDATION_MESSAGES.required,
            pattern: /^\S+@\S+$/i,
          })}
          margin="normal"
          sx={{ mt: 0 }}
        />
        <TextField
          disabled={disabled}
          error={!!errors.password}
          helperText={errors.password?.message}
          type="password"
          label="password"
          size="small"
          fullWidth
          autoComplete="new-password"
          {...register('password', {
            required: VALIDATION_MESSAGES.required,
            minLength: {
              value: 4,
              message: VALIDATION_MESSAGES.minLength(4),
            },
          })}
          margin="normal"
          sx={{ mt: 0 }}
        />
        <TextField
          disabled={disabled}
          error={!!errors.passwordConfirm}
          helperText={errors.passwordConfirm?.message}
          type="password"
          label="confirm password"
          size="small"
          fullWidth
          autoComplete="new-password"
          {...register('passwordConfirm', {
            required: VALIDATION_MESSAGES.required,
            validate: (value, formValues) =>
              new Promise((resolve) => {
                if (value && value !== formValues.password) {
                  return resolve(VALIDATION_MESSAGES.passwordsMatch);
                } else {
                  return resolve(true);
                }
              }),
          })}
          margin="normal"
          sx={{ mt: 0 }}
        />
        <LoadingButton
          onClick={handleSubmit(handleClickRegister)}
          loading={isLoading}
          disabled={disabled}
          type="submit"
        >
          register
        </LoadingButton>
      </Box>
    </TabPanelStyled>
  );
};
