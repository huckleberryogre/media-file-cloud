import { AppBar, Container, Paper, Tab, Tabs } from '@mui/material';
import React, { useCallback, useEffect } from 'react';
import { login } from '../../store/slice/auth';
import Cookie from 'js-cookie';
// todo replace react-swipeable-views with some alternative:
// https://github.com/mui/mui-x/issues/3601
// https://github.com/mui/material-ui/issues/33392
// https://swiperjs.com/
// note: don't forget to clean up reference in the react-app-env.d.ts
import SwipeableViews from 'react-swipeable-views';
import { LoginTab } from './components/LoginTab';
import { RegisterTab } from './components/RegisterTab';
import { useAppDispatch } from '../../store';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { IFeedback } from '@shared-models';
import { isFeedback } from '../../utilities/isFeedback';
import { showFeedback } from '../../api/interceptors';
import { useNavigateFromStateOrMainPage } from './hooks/useNavigateFromStateOrMainPage';

export enum ETab {
  LOGIN,
  REGISTER,
}

export const Login = () => {
  const navigate = useNavigate();
  const { navigateBack } = useNavigateFromStateOrMainPage();
  const { uuid } = useParams();
  const dispatch = useAppDispatch();
  const location = useLocation();
  const tab = location.pathname.includes('register')
    ? ETab.REGISTER
    : ETab.LOGIN;
  const registerUUID = location.state?.registerUUID;

  const triggerLoginFromCookie = useCallback(() => {
    dispatch({ type: login.fulfilled.type });
    navigateBack();
  }, [dispatch, navigateBack]);

  useEffect(() => {
    const handleMessage = (
      event: MessageEvent<{
        jwt: string;
        isLoginPopup?: boolean;
        feedback?: IFeedback;
      }>,
    ) => {
      if (event.data?.isLoginPopup) {
        const feedback = JSON.parse(Cookie.get('feedback') || 'null');

        if (feedback && isFeedback(feedback)) {
          showFeedback(feedback);
          if (feedback.success) {
            triggerLoginFromCookie();
          }

          Cookie.remove('feedback');
        }
      }
    };

    window.addEventListener('message', handleMessage);

    return () => {
      window.removeEventListener('message', handleMessage);
    };
  }, [dispatch, location.state, triggerLoginFromCookie]);

  const handleTabChange = (index: number) => {
    if (index === ETab.LOGIN) {
      navigate('/login', { replace: true, state: { registerUUID: uuid } });
    } else {
      if (registerUUID) {
        navigate(`/register/${registerUUID}`, { replace: true });
      } else {
        navigate('/register', { replace: true });
      }
    }
  };

  return (
    <Container maxWidth="xs" sx={{ alignSelf: 'center' }}>
      <Paper elevation={8} sx={{ width: '340px', margin: 'auto' }}>
        <AppBar position="static">
          <Tabs
            value={tab}
            onChange={(_, index) => handleTabChange(index)}
            indicatorColor="secondary"
            textColor="inherit"
            variant="fullWidth"
          >
            <Tab label="Login" tabIndex={ETab.LOGIN} />
            <Tab label="Register" tabIndex={ETab.REGISTER} />
          </Tabs>
        </AppBar>
        <SwipeableViews index={tab} onChangeIndex={handleTabChange}>
          <LoginTab activeTabIndex={tab} />
          <RegisterTab activeTabIndex={tab} />
        </SwipeableViews>
      </Paper>
    </Container>
  );
};
