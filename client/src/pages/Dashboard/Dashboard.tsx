import React from 'react';
import { Box } from '@mui/material';
import { FilesList } from 'components/FilesList/FilesList';

export const Dashboard = () => {
  return (
    <Box width="100%" data-testid="dashboard">
      <FilesList />
    </Box>
  );
};
