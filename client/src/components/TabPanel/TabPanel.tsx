import { Box } from '@mui/material';
import { BoxProps } from '@mui/material/Box/Box';

interface ITabPanelProps extends BoxProps {
  index: number;
  activeTabIndex: number;
}

export const TabPanel = ({
  children,
  activeTabIndex,
  index,
  ...other
}: ITabPanelProps) => {
  return activeTabIndex === index ? <Box {...other}>{children}</Box> : null;
};
