import { Route, Routes } from 'react-router-dom';
import { ProtectedRoute } from '../ProtectedRoute/ProtectedRoute';
import { Dashboard } from '../../pages/Dashboard/Dashboard';
import { Settings } from '../../pages/Settings/Settings';
import { FilePreview } from '../../pages/FilePreview/FilePreview';
import { Login } from '../../pages/Login/Login';
import { NotFound } from '../../pages/NotFound/NotFound';
import React from 'react';

export const Router = () => {
  return (
    <Routes>
      <Route path="/">
        <Route
          path="/shared/file/:uuid"
          element={<FilePreview shouldLoadSharedFileEntity />}
        />
        <Route element={<ProtectedRoute />}>
          <Route index element={<Dashboard />} />
          <Route path="/settings" element={<Settings />} />
          <Route path="/files/:uuid" element={<FilePreview />} />
        </Route>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Login />}>
          <Route index element={<Login />} />
          <Route path=":uuid" element={<Login />} />
        </Route>
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
};
