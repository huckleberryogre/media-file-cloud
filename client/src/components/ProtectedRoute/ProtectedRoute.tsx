import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectUser } from 'store/slice/auth';

export const ProtectedRoute = () => {
  const user = useSelector(selectUser);
  const location = useLocation();
  const from = location.pathname;

  return !user ? (
    <Navigate to="/login" state={{ from: from || '/' }} />
  ) : (
    <Outlet />
  );
};
