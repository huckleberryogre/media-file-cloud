import { Player } from '@lottiefiles/react-lottie-player';
import { BoxProps } from '@mui/material/Box/Box';
import { Box, Theme, useMediaQuery } from '@mui/material';
import { isWinterHolidays } from 'utilities/isWinterHolidays';
import { APP_BAR_HEIGHT, APP_BAR_HEIGHT_MOBILE } from '../styled';
import React, { useMemo } from 'react';
import SnowFlakesLottiePath from 'assets/lottie/snowflakes.json';

export const SnowflakesAnimation = () => {
  const isMobile = useMediaQuery<Theme>((theme) =>
    theme.breakpoints.down('sm'),
  );
  const show = isWinterHolidays();
  const SNOW_FLAKES_WIDTH = 300;
  const SnowflakesAnimationComponent = (props: BoxProps) => (
    <Box {...props} width={SNOW_FLAKES_WIDTH}>
      <Player src={SnowFlakesLottiePath} autoplay loop speed={0.2} />
    </Box>
  );

  const RepeatedSnowflakesAnimationComponent = useMemo(
    () =>
      Array.from({
        length: Math.ceil(window.innerWidth / SNOW_FLAKES_WIDTH),
      }).map((_, index) => <SnowflakesAnimationComponent key={index} />),
    [],
  );

  return show ? (
    <Box
      sx={{
        position: 'absolute',
        height: isMobile ? APP_BAR_HEIGHT_MOBILE : APP_BAR_HEIGHT,
        width: `100%`,
        overflow: 'hidden',
      }}
    >
      <Box display="flex">{RepeatedSnowflakesAnimationComponent}</Box>
    </Box>
  ) : null;
};
