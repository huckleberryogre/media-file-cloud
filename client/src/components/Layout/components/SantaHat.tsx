import { Box } from '@mui/material';
import SantaHatIconPath from 'assets/images/santa-hat.png';
import { isWinterHolidays } from 'utilities/isWinterHolidays';
import { SANTA_HAT_CLASS_NAME, santaHatStyle } from '../styled';

export const SantaHat = () => {
  const show = isWinterHolidays();
  const { img, main } = santaHatStyle.logo;

  return show ? (
    <Box className={SANTA_HAT_CLASS_NAME} sx={main}>
      <img src={SantaHatIconPath} alt="Santa Hat" width={img.width} />
    </Box>
  ) : null;
};
