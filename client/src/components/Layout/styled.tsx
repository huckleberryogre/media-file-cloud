import { AppBar, Avatar, Box, Drawer, Paper, styled } from '@mui/material';
import { DRAWER_WIDTH } from '../../constants';
import { EAppTheme } from '../../enums';
import { BoxProps } from '@mui/material/Box/Box';

export const APP_BAR_HEIGHT = 64;
export const APP_BAR_HEIGHT_MOBILE = 48;

export const LayoutPaperStyled = styled(Paper)(({ theme }) => ({
  minHeight: `calc(100vh - ${APP_BAR_HEIGHT}px)`,
  display: 'flex',
  padding: theme.spacing(2),
  width: '100%',
  [theme.breakpoints.down('sm')]: {
    minHeight: `calc(100vh - ${APP_BAR_HEIGHT_MOBILE}px)`,
  },
}));

export const MainBoxStyled = styled(Box)(({ theme }) => ({
  position: 'relative',
  flexGrow: 1,
  [theme.breakpoints.up('sm')]: {
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
  },
}));

export const MainAppBarStyled = styled(AppBar)(({ theme }) => ({
  position: 'absolute',
  [theme.breakpoints.up('sm')]: {
    ml: `${DRAWER_WIDTH}px`,
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
  },
}));

export const DrawerBoxStyled = styled(Box)(({ theme }) => ({
  [theme.breakpoints.up('sm')]: {
    width: `${DRAWER_WIDTH}px`,
    flexShrink: 0,
  },
}));

export const DrawerMobileStyled = styled(Drawer)(({ theme }) => ({
  [theme.breakpoints.up('sm')]: {
    display: 'none',
  },
  [theme.breakpoints.down('sm')]: {
    display: 'block',
  },
  '& .MuiDrawer-paper': {
    boxSizing: 'border-box',
    width: DRAWER_WIDTH,
  },
}));

export const DrawerDesktopStyled = styled(Drawer)(({ theme }) => ({
  [theme.breakpoints.up('sm')]: {
    display: 'block',
  },
  [theme.breakpoints.down('sm')]: {
    display: 'none',
  },
  '& .MuiDrawer-paper': {
    boxSizing: 'border-box',
    width: DRAWER_WIDTH,
  },
}));

export const AvatarStyled = styled(Avatar)<{ open: boolean }>(
  ({ theme, open }) => ({
    cursor: 'pointer',
    outline: open ? `2px solid ${theme.palette.secondary.main}` : 'none',
    '&:hover': {
      outline: `2px solid ${theme.palette.secondary.main}`,
    },
  }),
);

export const SANTA_HAT_CLASS_NAME = 'santa-hat';

export const LogoWithTitleWrapper = styled(
  ({ hover, ...props }: BoxProps & { hover: { transform: string } }) => (
    <Box {...props} />
  ),
)(({ theme, hover }) => ({
  cursor: 'pointer',
  '&:hover': {
    color:
      theme.palette.mode === EAppTheme.DARK
        ? theme.palette.primary.main
        : theme.palette.common.black,
  },
  '&:hover svg': {
    opacity: 0.8,
  },
  [`&:hover .${SANTA_HAT_CLASS_NAME}`]: {
    ...hover,
  },
}));

export const santaHatStyle = {
  header: {
    img: {
      width: '23',
    },
    main: {
      position: 'absolute',
      transform: 'scaleX(-1) translate3d(8px,-3px,0) rotate(30deg)',
      transition: 'transform 0.3s ease-in-out',
    },
    hover: {
      transform: 'scaleX(-1) translate3d(8px,-11px,0) rotate(0deg)',
    },
  },
  logo: {
    img: {
      width: '60',
    },
    main: {
      position: 'absolute',
      transform: 'scaleX(-1) translate3d(74px,-28px,0) rotate(25deg)',
      transition: 'transform 0.3s ease-in-out',
    },
    hover: {
      transform: 'scaleX(-1) translate3d(73px,-37px,0) rotate(10deg)',
    },
  },
};
