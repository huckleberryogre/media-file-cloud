import { useRef, useState } from 'react';
import {
  Box,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Toolbar,
  Tooltip,
  Typography,
} from '@mui/material';
import {
  LogoutRounded as LogoutIcon,
  MenuRounded as MenuIcon,
  SettingsRounded as SettingsIcon,
} from '@mui/icons-material';
import {
  AvatarStyled,
  LogoWithTitleWrapper,
  MainAppBarStyled,
  santaHatStyle,
} from './styled';
import { logout, selectUser } from 'store/slice/auth';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from 'store';
import { LogoSVG } from 'assets/logo/logo';
import { SnowflakesAnimation } from './components/SnowflakesAnimation';
import { SantaHat } from './components/SantaHat';

interface IProps {
  onDrawerToggle: () => void;
}

export const AppBar = ({ onDrawerToggle }: IProps) => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const user = useSelector(selectUser);
  const avatarRef = useRef(null);
  const [open, setOpen] = useState(false);

  const handleClickLogout = () => {
    dispatch(logout());
    navigate('/login');
    setOpen(false);
  };

  const { hover } = santaHatStyle.logo;

  return (
    <MainAppBarStyled>
      <SnowflakesAnimation />
      <Toolbar>
        <Box display="flex" height="100%" width="100%">
          <Box display="flex" alignItems="center">
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={onDrawerToggle}
              sx={{ mr: 2, display: { sm: 'none' } }}
            >
              <MenuIcon />
            </IconButton>
            <LogoWithTitleWrapper
              display="flex"
              alignItems="center"
              onClick={() => navigate('/')}
              hover={hover}
            >
              <LogoSVG width={40} height={40} style={{ marginRight: '10px' }} />
              <Typography variant="h6" noWrap component="div">
                <SantaHat />
                Media File Cloud
              </Typography>
            </LogoWithTitleWrapper>
          </Box>
          <Box marginLeft="auto">
            <Tooltip title={user?.email || user?.username} placement="bottom">
              <AvatarStyled
                open={open}
                src={user?.avatar}
                ref={avatarRef}
                onClick={() => user && setOpen(true)}
                alt={user?.username}
              />
            </Tooltip>
            <Menu
              id="user-menu"
              anchorEl={avatarRef.current}
              open={open}
              onClose={() => setOpen(false)}
              sx={{
                transform: 'translateY(10px)',
              }}
            >
              <MenuItem onClick={() => navigate('settings')}>
                <ListItemIcon>
                  <SettingsIcon />
                </ListItemIcon>
                <ListItemText primary="Settings" />
              </MenuItem>
              <MenuItem onClick={handleClickLogout}>
                <ListItemIcon>
                  <LogoutIcon />
                </ListItemIcon>
                <ListItemText primary="Logout" />
              </MenuItem>
            </Menu>
          </Box>
        </Box>
      </Toolbar>
    </MainAppBarStyled>
  );
};
