import { Box, Paper, Toolbar } from '@mui/material';
import React from 'react';
import { Drawer } from './Drawer';
import { AppBar } from './AppBar';
import { LayoutPaperStyled, MainBoxStyled } from './styled';
import { useSelector } from 'react-redux';
import { selectIsDrawerOpen, setIsDrawerOpen } from 'store/slice/settings';
import { useAppDispatch } from 'store';

interface IProps {
  children: React.ReactNode;
}

export const MainLayout = ({ children }: IProps) => {
  const dispatch = useAppDispatch();
  const isDrawerOpen = useSelector(selectIsDrawerOpen);
  const handleDrawerToggle = () => {
    dispatch(setIsDrawerOpen(!isDrawerOpen));
  };

  return (
    <Paper square elevation={0} sx={{ minHeight: '100vh' }}>
      <Box sx={{ display: 'flex' }}>
        <AppBar onDrawerToggle={handleDrawerToggle} />
        <Drawer
          isMobileDrawerOpen={isDrawerOpen}
          onDrawerToggle={handleDrawerToggle}
        />
        <MainBoxStyled component="main">
          <Toolbar />
          <LayoutPaperStyled>{children}</LayoutPaperStyled>
        </MainBoxStyled>
      </Box>
    </Paper>
  );
};
