import { useNavigate } from 'react-router-dom';
import {
  Box,
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar,
} from '@mui/material';
import {
  DashboardRounded as DashboardIcon,
  DriveFolderUploadRounded as DriveFolderUploadIcon,
  LoginRounded as LoginIcon,
  UploadFileRounded as UploadFileIcon,
} from '@mui/icons-material';
import React, { ChangeEvent } from 'react';
import { FilesAPI } from 'api';

const ROUTES = [
  {
    to: '/',
    icon: <DashboardIcon />,
    text: 'Dashboard',
  },
  {
    to: '/login',
    icon: <LoginIcon />,
    text: 'Login',
  },
];

export const NavList = () => {
  const navigate = useNavigate();

  const handleClickListItem = (to: string) => () => {
    navigate(to);
  };

  const handleChangeFileInput = async (
    event: ChangeEvent<HTMLInputElement>,
  ) => {
    const fileList = event.target.files;

    if (fileList) {
      console.log(fileList);
      await FilesAPI.upload(fileList);
    }
  };

  return (
    <>
      <Toolbar />
      <Divider />
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          flexGrow: '1',
          justifyContent: 'space-between',
        }}
      >
        <List>
          {ROUTES.map(({ to, icon, text }) => (
            <ListItem key={text} disablePadding>
              <ListItemButton onClick={handleClickListItem(to)}>
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
        <List disablePadding>
          <Divider />
          <ListItem disablePadding>
            <ListItemButton component="label">
              <ListItemIcon sx={{ minWidth: '36px' }}>
                <UploadFileIcon color="primary" />
              </ListItemIcon>
              <ListItemText
                primary="upload files"
                sx={{ textAlign: 'center' }}
              />
              <input
                hidden
                multiple
                type="file"
                onChange={handleChangeFileInput}
              />
            </ListItemButton>
            <Divider flexItem orientation="vertical" />
            <ListItemButton component="label">
              <ListItemIcon sx={{ minWidth: '30px' }}>
                <DriveFolderUploadIcon color="secondary" />
              </ListItemIcon>
              <ListItemText
                primary="upload folder"
                sx={{ textAlign: 'center' }}
              />
              <input
                hidden
                multiple
                type="file"
                //@ts-expect-error
                webkitdirectory="true"
                onChange={handleChangeFileInput}
              />
            </ListItemButton>
          </ListItem>
        </List>
      </Box>
    </>
  );
};
