import { Breadcrumbs, Link } from '@mui/material';
import { HomeRounded as HomeIcon } from '@mui/icons-material';

interface IProps {
  folder: string[];
  setFolder: React.Dispatch<React.SetStateAction<string[]>>;
  handleClickBreadcrumb: (index: number) => () => void;
}

const linkStyle = {
  display: 'flex',
  alignItems: 'center',
  cursor: 'pointer',
};

export const FilesListBreadcrumbs = ({
  folder,
  setFolder,
  handleClickBreadcrumb,
}: IProps) => {
  return (
    <Breadcrumbs aria-label="breadcrumb">
      <Link
        color={folder.length ? 'inherit' : 'text.primary'}
        sx={linkStyle}
        onClick={() => setFolder([])}
        underline="hover"
      >
        <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
        root
      </Link>
      {folder.map((folderName, index) => (
        <Link
          key={folderName}
          color={folder.length - 1 === index ? 'text.primary' : 'inherit'}
          onClick={handleClickBreadcrumb(index)}
          underline="hover"
          sx={linkStyle}
        >
          {folderName}
        </Link>
      ))}
    </Breadcrumbs>
  );
};
