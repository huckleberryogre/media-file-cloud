import {
  Chip,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Stack,
  Tooltip,
  useTheme,
} from '@mui/material';
import {
  AudioFileRounded as AudioFileIcon,
  DeleteRounded as DeleteIcon,
  FolderRounded as FolderIcon,
  ImageRounded as ImageIcon,
  InsertDriveFileRounded as InsertDriveFileIcon,
  LinkRounded as LinkIcon,
} from '@mui/icons-material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {
  selectAllFilesEntries,
  selectCurrentLevelSubdirectories,
  selectFoldersMap,
} from 'store/slice/files';
import { get, isEqual, take, uniq } from 'lodash';
import { IFileEntry } from 'models';
import { FilesAPI } from 'api';
import { useAppDispatch } from 'store';
import { FilesListBreadcrumbs } from './FilesListBreadcrumbs';
import { notificationActions } from 'store/slice/notifications';
import { EFileVisibility } from 'enums';

export const FilesList = () => {
  const dispatch = useAppDispatch();
  const theme = useTheme();
  const navigate = useNavigate();
  const [folder, setFolder] = useState<string[]>([]);
  const filesEntries = useSelector(selectAllFilesEntries);
  const foldersMap = useSelector(selectFoldersMap);
  const subDirectories = useSelector(selectCurrentLevelSubdirectories(folder));
  const subFolders = uniq([
    ...Object.keys(folder.length ? get(foldersMap, folder, {}) : foldersMap),
    ...subDirectories,
  ]);
  const filesInFolder = filesEntries.filter((file) =>
    folder.length
      ? isEqual(take(folder, file.path.length + 1), file.path)
      : !file.path.length,
  );

  const handleClickDelete = (fileEntry: IFileEntry) => async () => {
    await FilesAPI.remove(fileEntry.id);
  };

  const handleFolderClick = (subFolder: string) => () =>
    setFolder([...folder, subFolder]);
  const handleClickBreadcrumb = (index: number) => () => {
    setFolder(take(folder, index + 1));
  };

  const handleFileClick = (fileEntry: IFileEntry) => () => {
    navigate(`/files/${fileEntry.uuid}`);
  };

  const handleClickCopyLink = (fileEntry: IFileEntry) => () => {
    navigator.clipboard.writeText(
      `${window.location.origin}/api/files/${fileEntry.uuid}`,
    );
    dispatch(
      notificationActions.show({
        message: 'Link copied',
        severity: 'info',
        duration: 2000,
      }),
    );

    if (fileEntry.visibility !== EFileVisibility.PUBLIC) {
      FilesAPI.setVisibility(fileEntry.uuid, EFileVisibility.PUBLIC);
    }
  };

  const handleClickVisibility =
    (fileEntry: IFileEntry) => async (event: React.MouseEvent) => {
      event.stopPropagation();
      await FilesAPI.setVisibility(fileEntry.uuid, EFileVisibility.PRIVATE);
    };

  const hoverStyles = {
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
      cursor: 'pointer',
    },
  };

  return (
    <List
      subheader={
        <FilesListBreadcrumbs
          folder={folder}
          setFolder={setFolder}
          handleClickBreadcrumb={handleClickBreadcrumb}
        />
      }
    >
      {filesInFolder.map((file) => (
        <ListItem sx={hoverStyles} key={file.uuid}>
          <ListItemIcon>
            {file.mimeType.startsWith('audio/') ? (
              <AudioFileIcon />
            ) : file.mimeType.startsWith('image/') ? (
              <ImageIcon />
            ) : (
              <InsertDriveFileIcon />
            )}
          </ListItemIcon>
          <ListItemText onClick={handleFileClick(file)}>
            {file.name}
            {file.visibility === EFileVisibility.PUBLIC && (
              <Chip
                label="Public"
                size="small"
                color="info"
                sx={(theme) => ({
                  marginLeft: theme.spacing(1),
                })}
                onClick={handleClickVisibility(file)}
              />
            )}
          </ListItemText>
          <Stack spacing={2} direction="row" alignItems="center">
            <Tooltip title="Copy link">
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={handleClickCopyLink(file)}
                sx={{ py: 0 }}
              >
                <LinkIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete file">
              <IconButton
                edge="end"
                aria-label="delete"
                color="error"
                onClick={handleClickDelete(file)}
                sx={{ py: 0 }}
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </Stack>
        </ListItem>
      ))}
      {subFolders.map((subFolder) => (
        <ListItem
          sx={hoverStyles}
          key={subFolder}
          onClick={handleFolderClick(subFolder)}
        >
          <ListItemIcon>
            <FolderIcon color="secondary" />
          </ListItemIcon>
          <ListItemText primary={subFolder} />
        </ListItem>
      ))}
    </List>
  );
};
