import { IconButton } from '@mui/material';
import {
  CloseRounded as CloseIcon,
  ErrorOutline as ErrorIcon,
  InfoOutlined as InfoIcon,
  ReportProblemOutlined as ReportProblemIcon,
  TaskAltOutlined as TaskAltIcon,
} from '@mui/icons-material';
import React from 'react';

interface IActionButtonProps {
  onClick: () => void;
}

export const NotistackActionButton = ({ onClick }: IActionButtonProps) => {
  return (
    <IconButton
      aria-label="close"
      onClick={onClick}
      sx={(theme) => ({
        color: theme.palette.common.white,
      })}
    >
      <CloseIcon />
    </IconButton>
  );
};

export const ICON_VARIANTS = {
  default: <InfoIcon sx={{ mr: 1 }} />,
  error: <ErrorIcon sx={{ mr: 1 }} />,
  success: <TaskAltIcon sx={{ mr: 1 }} />,
  warning: <ReportProblemIcon sx={{ mr: 1 }} />,
  info: <InfoIcon sx={{ mr: 1 }} />,
};
