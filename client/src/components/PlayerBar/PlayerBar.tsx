import {
  MouseEvent,
  MouseEventHandler,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  Box,
  IconButton,
  LinearProgress,
  Paper,
  Slider,
  Stack,
  TextField,
  Theme,
  Tooltip,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import {
  ExpandLessRounded as ExpandLessIcon,
  ExpandMoreRounded as ExpandMoreIcon,
  PauseRounded as PauseIcon,
  PlayArrowRounded as PlayArrowIcon,
  SkipNextRounded as SkipNextIcon,
  SkipPreviousRounded as SkipPreviousIcon,
  VolumeDownRounded as VolumeDownIcon,
  VolumeOffRounded as VolumeMuteIcon,
  VolumeUpRounded as VolumeUpIcon,
} from '@mui/icons-material';
import { Howl } from 'howler';
import { getFileUrl } from 'store/slice/files';
import { useAppDispatch } from 'store';
import {
  selectIsPlayerBarExpanded,
  setIsPlayerBarExpanded,
} from 'store/slice/settings';
import { useSelector } from 'react-redux';
import {
  selectCurrentTrackFileEntry,
  selectIsPlaying,
  selectTriggerPlayFromStart,
  setIsPlaying,
} from 'store/slice/player';
import { DRAWER_WIDTH } from '../../constants';

const PROGRESS_BAR_HEIGHT = 10;
const PLAYER_BAR_HEIGHT = 65;

/**
 * A player bar component with the play/stop button and the progress bar.
 * The bar is fixed at the bottom of the screen, takes up the whole width and 15% of the height.
 */
export const PlayerBar = () => {
  const theme = useTheme();
  const isPlayerBarExpanded = useSelector(selectIsPlayerBarExpanded);
  const fileEntry = useSelector(selectCurrentTrackFileEntry);
  const triggerPlayFromStart = useSelector(selectTriggerPlayFromStart);
  const dispatch = useAppDispatch();
  const isPlaying = useSelector(selectIsPlaying);
  const sound = useRef<Howl>();
  const interval = useRef<NodeJS.Timeout>();
  const [progress, setProgress] = useState(0);
  const [hoverProgress, setHoverProgress] = useState(0);
  const [volume, setVolume] = useState(1);
  const [muted, setMuted] = useState(false);
  const expandButtonStyle = useMemo(
    () => ({
      position: 'fixed',
      marginLeft: theme.spacing(2),
      right: 16,
      bottom: isPlayerBarExpanded ? 9 : 12,
      transition: 'bottom 0.3s ease-in-out',
    }),
    [isPlayerBarExpanded, theme],
  );
  const isMobile = useMediaQuery<Theme>((theme) =>
    theme.breakpoints.down('sm'),
  );

  const handleClickTogglePlay = useCallback(
    (play: boolean) => {
      if (sound.current) {
        if (play) {
          sound.current?.play();
          interval.current = setInterval(() => {
            setProgress(sound.current!.seek() / sound.current!.duration() || 0);
          }, 30);
        } else {
          sound.current?.pause();
          clearInterval(interval.current as NodeJS.Timeout);
        }

        dispatch(setIsPlaying(play));
      }
    },
    [dispatch],
  );

  useEffect(() => {
    interval.current && clearInterval(interval.current as NodeJS.Timeout);

    if (fileEntry) {
      dispatch(getFileUrl(fileEntry)).then((data) => {
        sound.current = new Howl({
          src: [data.payload as string],
          format: [fileEntry.extension],
          volume,
        });
        sound.current.on('end', () => dispatch(setIsPlaying(false)));

        handleClickTogglePlay(true);
      });
    }

    return () => {
      sound.current?.unload();
      interval.current && clearInterval(interval.current as NodeJS.Timeout);
      dispatch(setIsPlaying(false));
      setProgress(0);
      setHoverProgress(0);
    };
    // eslint-disable-next-line
  }, [fileEntry, dispatch, handleClickTogglePlay, triggerPlayFromStart]);

  const getCursorPositionPercentage = (event: MouseEvent<HTMLDivElement>) => {
    const rect = event.currentTarget.getBoundingClientRect();
    const x = event.clientX - rect.left;

    return x / rect.width;
  };

  const handleClickProgress = (event: MouseEvent<HTMLDivElement>) => {
    const cursorPositionPercentage = getCursorPositionPercentage(event);
    setProgress(cursorPositionPercentage);
    sound.current?.seek(sound.current.duration() * cursorPositionPercentage);
  };

  const handleHover = (event: MouseEvent<HTMLDivElement>) => {
    const percentage = getCursorPositionPercentage(event);
    setHoverProgress(percentage);
  };

  // fullTimePast - seconds
  const fullTimePast = sound.current ? sound.current?.seek() : 0;

  const getCurrentTime = (timePast = fullTimePast) => {
    const currentTimeSeconds = Math.floor(timePast % 60);
    const currentTimeMinutes = Math.floor((timePast / 60) % 60);
    const currentTimeHours = Math.floor(timePast / 3600);

    return `${currentTimeHours < 10 ? '0' : ''}${currentTimeHours}:${
      currentTimeMinutes < 10 ? '0' : ''
    }${currentTimeMinutes}:${
      currentTimeSeconds < 10 ? '0' : ''
    }${currentTimeSeconds}`;
  };

  const handleChangeVolume = (value: number) => {
    const newValue = value / 100;

    sound.current?.volume(newValue);
    setVolume(newValue);

    if (newValue === 0) {
      setMuted(true);
    } else {
      setMuted(false);
    }
  };

  const handleClickVolumeIcon = useCallback(() => {
    volume === 0 && handleChangeVolume(10);
    setMuted(!muted);
    sound.current?.mute(!muted);
  }, [muted, volume]);

  const VolumeIcon = useMemo(
    () =>
      ({ onClick }: { onClick: MouseEventHandler<HTMLOrSVGElement> }) => {
        if (volume === 0 || muted) {
          return (
            <VolumeMuteIcon
              onClick={onClick}
              sx={{ color: 'error.main', cursor: 'pointer' }}
            />
          );
        } else if (volume < 0.5) {
          return (
            <VolumeDownIcon
              onClick={onClick}
              sx={{ color: 'text.primary', cursor: 'pointer' }}
            />
          );
        } else {
          return (
            <VolumeUpIcon
              onClick={onClick}
              sx={{ color: 'text.primary', cursor: 'pointer' }}
            />
          );
        }
      },
    [volume, muted],
  );

  const handleClickExpand = () => {
    dispatch(setIsPlayerBarExpanded(!isPlayerBarExpanded));
  };

  return (
    <>
      <Box
        sx={{
          position: 'fixed',
          bottom: 0,
          left: isMobile ? 0 : DRAWER_WIDTH,
          right: 0,
          height: `${PLAYER_BAR_HEIGHT}px`,
          transform: `translate3d(0, ${
            isPlayerBarExpanded ? 0 : PLAYER_BAR_HEIGHT - PROGRESS_BAR_HEIGHT
          }px, 0)`,
          transition: 'transform 0.3s ease-in-out',
        }}
      >
        <Paper sx={{ height: '100%' }}>
          <Tooltip
            title={getCurrentTime(
              sound.current ? hoverProgress * sound.current.duration() : 0,
            )}
            placement="top"
            followCursor
          >
            <LinearProgress
              sx={{
                height: PROGRESS_BAR_HEIGHT,
                cursor: 'pointer',
                '& .MuiLinearProgress-bar': {
                  transition: 'none',
                },
              }}
              variant="determinate"
              value={(hoverProgress || progress) * 100}
              onClick={handleClickProgress}
              onMouseEnter={handleHover}
              onMouseMove={handleHover}
              onMouseLeave={() => setHoverProgress(0)}
            />
          </Tooltip>
          <Box
            sx={{
              height: `calc(100% - ${PROGRESS_BAR_HEIGHT}px)`,
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              position: 'relative',
            }}
          >
            <Box
              sx={{
                height: '100%',
                display: 'flex',
                position: 'absolute',
                top: 0,
                left: 0,
                alignItems: 'center',
              }}
            >
              <Stack
                spacing={2}
                direction="row"
                sx={{ mb: 1, ml: theme.spacing(2) }}
                alignItems="center"
              >
                <VolumeIcon onClick={handleClickVolumeIcon} />
                <Slider
                  key={'volume'}
                  aria-label="Volume"
                  value={muted ? 0 : Math.round(volume * 100)}
                  onChange={(_, value) => handleChangeVolume(value as number)}
                  size="small"
                  sx={{ width: '100px' }}
                  color="secondary"
                  valueLabelDisplay="auto"
                  onWheel={(event) => {
                    const delta = event.deltaY;
                    const newValue =
                      delta > 0 ? volume * 100 - 10 : volume * 100 + 10;

                    if (newValue < 0) {
                      handleChangeVolume(0);
                      setMuted(true);
                    } else if (newValue > 100) {
                      handleChangeVolume(100);
                    } else {
                      handleChangeVolume(newValue);
                    }

                    if (delta < 0 && muted) {
                      handleChangeVolume(10);
                      setMuted(false);
                      sound.current?.mute(false);
                    }

                    event.preventDefault();
                  }}
                />
              </Stack>
            </Box>
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <IconButton
                size="medium"
                aria-label="previous"
                disabled={!fileEntry}
              >
                <SkipPreviousIcon />
              </IconButton>
              <IconButton
                aria-label="play/pause"
                onClick={() => handleClickTogglePlay(!isPlaying)}
                color={isPlaying ? 'warning' : 'success'}
                size="large"
                disabled={!fileEntry}
              >
                {isPlaying ? <PauseIcon /> : <PlayArrowIcon />}
              </IconButton>
              <IconButton size="medium" aria-label="next" disabled={!fileEntry}>
                <SkipNextIcon />
              </IconButton>
            </Box>
            <Box
              sx={{
                height: '100%',
                display: 'flex',
                position: 'absolute',
                top: 0,
                right: 0,
                alignItems: 'center',
                padding: theme.spacing(0, 2),
              }}
            >
              <TextField
                sx={{
                  marginRight: theme.spacing(2),
                  width: '58px',
                }}
                size="small"
                value={getCurrentTime()}
                variant="standard"
                disabled
              />
              /
              <TextField
                sx={{
                  marginLeft: theme.spacing(2),
                  width: '58px',
                }}
                size="small"
                value={getCurrentTime(sound.current?.duration())}
                variant="standard"
                disabled
              />
              <IconButton
                size="medium"
                aria-label="expand"
                sx={{ marginLeft: theme.spacing(2), visibility: 'hidden' }}
                onClick={handleClickExpand}
              >
                {isPlayerBarExpanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
              </IconButton>
            </Box>
          </Box>
        </Paper>
      </Box>
      <IconButton
        size="medium"
        aria-label="expand"
        sx={expandButtonStyle}
        onClick={handleClickExpand}
      >
        {isPlayerBarExpanded ? <ExpandMoreIcon /> : <ExpandLessIcon />}
      </IconButton>
    </>
  );
};
