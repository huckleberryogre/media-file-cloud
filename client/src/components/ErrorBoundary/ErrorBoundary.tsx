import React from 'react';
import { IconButton } from '@mui/material';
import { RefreshRounded as RefreshIcon } from '@mui/icons-material';

interface ErrorBoundaryState {
  hasError: boolean;
  attempt: number;
}

export class ErrorBoundary extends React.Component<
  {
    children: React.ReactNode;
  },
  ErrorBoundaryState
> {
  state = { hasError: false, attempt: 0 };

  static getDerivedStateFromError(error: any) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error: any, errorInfo: any) {
    // You can also log the error to an error reporting service
    console.error('Caught an error:', error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <h3>
          Something went wrong. Try again
          <sup>{this.state.attempt > 0 && this.state.attempt}</sup>:{' '}
          <IconButton
            onClick={() =>
              this.setState({
                hasError: false,
                attempt: this.state.attempt + 1,
              })
            }
            color="primary"
            aria-label="refresh"
          >
            <RefreshIcon />
          </IconButton>
        </h3>
      );
    }

    return this.props.children;
  }
}
