import { Box } from '@mui/material';
import { Delete as DeleteIcon } from '@mui/icons-material';
import { TagsAPI } from 'api/tagsAPI';
import { getAllTags } from 'store/slice/tags';
import * as React from 'react';
import { useAppDispatch } from 'store';
import { ITag } from '../../models';

interface IProps {
  option: string;
  dispatch: ReturnType<typeof useAppDispatch>;
  allTags: ITag[];
  props: React.HTMLAttributes<HTMLLIElement>;
}

export const TagsOption = ({ option, dispatch, allTags, props }: IProps) => {
  return (
    <Box
      {...props}
      component="li"
      sx={{
        display: 'flex',
        justifyContent: 'space-between !important',
        alignItems: 'center !important',
      }}
    >
      <span>{option}</span>
      <DeleteIcon
        color="warning"
        sx={{ '&:hover': { color: 'error.main' } }}
        onClick={(event) => {
          const tag = allTags.find((tag) => tag.name === option);

          if (tag) {
            TagsAPI.deleteTag(tag.id).then(() => {
              dispatch(getAllTags());
            });
          }
          event.stopPropagation();
        }}
      />
    </Box>
  );
};
