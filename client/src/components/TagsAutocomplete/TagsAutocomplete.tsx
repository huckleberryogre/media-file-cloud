import { Autocomplete, TextField, useTheme } from '@mui/material';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { getAllTags, selectAllTagNames, selectAllTags } from 'store/slice/tags';
import { TagsAPI } from 'api/tagsAPI';
import { IFileEntry } from 'models';
import { useAppDispatch } from 'store';
import { TagsOption } from './TagsOption';

interface IProps {
  fileEntry: IFileEntry;
}

export const TagsAutocomplete = ({ fileEntry }: IProps) => {
  const dispatch = useAppDispatch();
  const allTags = useSelector(selectAllTags);
  const theme = useTheme();
  const allTagNames = useSelector(selectAllTagNames);
  const [fileTags, setFileTags] = useState(
    fileEntry?.tags.map((tag) => tag.name) || [],
  );

  useEffect(() => {
    if (fileEntry) {
      setFileTags(fileEntry.tags.map((tag) => tag.name));
    }
  }, [fileEntry]);

  const handleChangeTags = async (
    event: React.ChangeEvent<{}>,
    value: string[],
  ) => {
    setFileTags(value);
    if (fileEntry) {
      await TagsAPI.updateFileTags(fileEntry.uuid, value);
      dispatch(getAllTags());
    }
  };

  return (
    <Autocomplete
      multiple
      id="tags-standard"
      value={fileTags}
      options={allTagNames}
      ChipProps={{ color: 'secondary', size: 'small' }}
      renderOption={(props, option) => (
        <TagsOption
          key={option}
          dispatch={dispatch}
          allTags={allTags}
          option={option}
          props={props}
        />
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          variant="standard"
          label="File tags"
          placeholder="add tag"
          sx={{
            width: 'auto',
            minWidth: '200px',
          }}
          inputProps={{
            ...params.inputProps,
            sx: {
              marginLeft: theme.spacing(1),
              minWidth: '50px !important',
            },
          }}
        />
      )}
      onChange={handleChangeTags}
      freeSolo
      fullWidth
    />
  );
};
