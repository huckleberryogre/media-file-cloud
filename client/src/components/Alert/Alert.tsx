import { AlertProps } from '@mui/material';
import { AlertStyled } from './AlertStyled';

export const Alert = (props: AlertProps) => {
  return <AlertStyled {...props} />;
};
