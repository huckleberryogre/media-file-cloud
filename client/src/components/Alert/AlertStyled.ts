import { Alert, styled } from '@mui/material';

export const AlertStyled = styled(Alert)(({ icon }) => ({
  '> .MuiAlert-icon': {
    display: icon === null ? 'none' : 'flex',
  },
}));
