import axios from './axios';
import { ITag } from 'models';

/**
 * Tags API.
 */
export const TagsAPI = {
  getAll: () => {
    return axios.get('/tags');
  },
  updateFileTags: (fileUUID: string, tags: (string | ITag)[]) => {
    return axios.post(`/tags/update`, { fileUUID, tags });
  },
  deleteTag: (tagId: number) => {
    return axios.delete(`/tags/${tagId}`);
  },
};
