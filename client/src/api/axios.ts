import axios from 'axios';

/**
 * Axios instance.
 */
const instance = axios.create({
  baseURL: '/api',
});

export default instance;
