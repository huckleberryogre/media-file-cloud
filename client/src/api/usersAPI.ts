import axios from './axios';

export const UsersAPI = {
  getFeatures: () => {
    return axios.get('/users/features');
  },
};
