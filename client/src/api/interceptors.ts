// this file should be imported in the entrypoint (see index.tsx in the root)
import { AxiosError, AxiosResponse } from 'axios';
import axios from './axios';
import { isFeedback } from 'utilities/isFeedback';
import { store } from '../store';
import { notificationActions } from '../store/slice/notifications';
import { AlertColor } from '@mui/material';
import { IFeedback } from '@shared-models';
import Cookie from 'js-cookie';

type IFulfilledHandler = (response: AxiosResponse) => AxiosResponse;

export const showFeedback = (feedback: IFeedback) => {
  for (const severity in feedback) {
    const messageElement = feedback[severity as keyof IFeedback]!;
    const messageArray =
      messageElement instanceof Array ? messageElement : [messageElement];

    messageArray.forEach((message) => {
      store.dispatch(
        notificationActions.show({
          severity: severity as AlertColor,
          message,
        }),
      );
    });
  }
};

/**
 * Triggers notifications if any supplied within a backend response.
 * If request failed provides a feedback.
 *
 * @param data Response/error object.
 */
const handleFeedbackMessageShow = (data: AxiosResponse | AxiosError) => {
  let feedback =
    ((data as AxiosResponse).data?.feedback as IFeedback) ||
    (data as AxiosError<{ feedback: IFeedback }>).response?.data?.feedback ||
    {};
  const message = (data as AxiosError<{ message: string | string[] }>).response
    ?.data?.message;
  const additionalErrorMessages = [];

  // forming a feedback object if a string or an array of strings is supplied
  if (!!message) {
    if (typeof message === 'string') {
      additionalErrorMessages.push(
        `${(data as AxiosError).response?.status || ''} ${message}`,
      );
    } else if (
      Array.isArray(message) &&
      message.every((f) => typeof f === 'string')
    ) {
      additionalErrorMessages.push(...message);
    }
  }

  // adding additional error messages to the feedback object
  if (additionalErrorMessages.length > 0) {
    let errors: string[] = [];

    if (isFeedback(feedback) && feedback.error) {
      errors = Array.isArray(feedback.error)
        ? feedback.error
        : [feedback.error];
    }

    feedback = {
      ...feedback,
      error: [...errors, ...additionalErrorMessages],
    };
  }

  // showing a notification for each feedback message
  if (isFeedback(feedback)) {
    showFeedback(feedback);
  } else if (data instanceof AxiosError) {
    // fallback for a failed request if no feedback is supplied
    store.dispatch(
      notificationActions.show({
        severity: 'error',
        message: `${data.response?.status || ''} request failed`,
      }),
    );
  }

  return data instanceof AxiosError ? Promise.reject(data) : data;
};

// Add a response interceptor, see https://axios-http.com/docs/interceptors
axios.interceptors.response.use(
  handleFeedbackMessageShow as IFulfilledHandler,
  handleFeedbackMessageShow,
);

// Add a request interceptor to add a bearer token to all requests
axios.interceptors.request.use((config) => {
  const token = Cookie.get('token');

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  return config;
});
