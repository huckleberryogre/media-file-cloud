import { AxiosResponse } from 'axios';
import axios from './axios';
import { IDirectory, IFileEntry } from 'models';
import { IFeedback } from '@shared-models';
import { EFileVisibility } from 'enums';
import { get, set } from 'idb-keyval';

/**
 * Files API.
 */
export const FilesAPI = {
  async getFile(fileEntry: IFileEntry) {
    const { uuid, hash } = fileEntry;

    const cachedFile = await get(uuid);

    if (cachedFile?.file && cachedFile?.hash === hash) {
      return cachedFile.file;
    } else {
      const response = await axios.get(`/files/${fileEntry.uuid}`, {
        responseType: 'blob',
      });
      await set(uuid, { file: response.data, hash });

      return response.data;
    }
  },

  upload: async (files: FileList) => {
    const formData = new FormData();

    for (let index = 0; index < files.length; index++) {
      const file = files.item(index);
      const isHidden =
        file &&
        (file.webkitRelativePath
          .split(/[\\/]/)
          .some((part) => part.startsWith('.')) ||
          file.name.startsWith('.'));

      if (file && !isHidden) {
        formData.append(file.webkitRelativePath || file.name, file);
      }
    }

    return await axios.post('/files/upload', formData);
  },

  getAllFilesEntries: async (): Promise<AxiosResponse<IFileEntry[]>> => {
    const fileEntries = await axios.get('/files/entries');

    return {
      ...fileEntries,
      // filter out hidden files and folders
      data: fileEntries.data.filter((fileEntry: IFileEntry) => {
        const isHidden =
          fileEntry.path.some((dir) => dir.startsWith('.')) ||
          fileEntry.name.startsWith('.');
        return !isHidden;
      }),
    };
  },

  getSharedFilesEntries: async (
    uuid: string,
  ): Promise<AxiosResponse<IFileEntry[]>> => {
    const fileEntries = await axios.get(`/files/shared/${uuid}`);

    return {
      ...fileEntries,
      // filter out hidden files and folders
      data: fileEntries.data.filter((fileEntry: IFileEntry) => {
        const isHidden =
          fileEntry.path.some((dir) => dir.startsWith('.')) ||
          fileEntry.name.startsWith('.');
        return !isHidden;
      }),
    };
  },

  remove: (id: string | number) => {
    return axios.delete<IFeedback>(`/files/${id}`);
  },

  setVisibility: (
    uuid: string,
    visibility: EFileVisibility,
  ): Promise<AxiosResponse<IFeedback>> => {
    return axios.post('/files/set-visibility', {
      uuid,
      visibility,
    });
  },

  recalculateAllMetadata: (): Promise<AxiosResponse<IFeedback>> => {
    return axios.post('/files/recalculate-metadata');
  },

  getAllDirectoryEntries: (): Promise<AxiosResponse<IDirectory[]>> => {
    return axios.get('/files/directories');
  },
};
