import axios from './axios';

export interface CreateUserDtoClient {
  email: string;
  password: string;
  username?: string;
}

export interface LoginLocalDto {
  email: string;
  password: string;
}

/**
 * Authentication API.
 */
export const AuthAPI = {
  register: (user: CreateUserDtoClient, invitationUUID: string) => {
    return axios.post(`/auth/register?invitationUUID=${invitationUUID}`, user);
  },
  login: (credentials: LoginLocalDto): Promise<void> => {
    return axios.post('/auth/login', credentials).then((_) => {});
  },
  createRegistrationInvite: () => {
    return axios.post<{ uuid: string }>('/auth/create-registration-invite');
  },
};
