import {
  TAudioMetadataClientModel,
  TImageMetadataClientModel,
  TVideoMetadataClientModel,
} from '@shared-models';
import { EFileVisibility, ERequestStatus } from './enums';
import { AlertColor } from '@mui/material';

/**
 * Asynchronous response data model.
 */
export interface IAsyncData<T> {
  data: T;
  status: ERequestStatus;
}

export interface ITag {
  id: number;
  name: string;
}

/**
 * Client side file record model.
 */
export interface IFileEntry {
  id: number;
  created_at: string;
  updated_at: string;
  uuid: string;
  name: string;
  hash: string;
  metadata:
    | TImageMetadataClientModel
    | TAudioMetadataClientModel
    | TVideoMetadataClientModel;
  size: number;
  mimeType: string;
  extension: string;
  // file path relative to the root folder (e.g. "folder1/folder2/file.txt")
  // comes in chunks from the server, e.g. ["folder1", "folder2", "file.txt"]
  // this is used to build the file tree
  path: string[];
  tags: ITag[];
  visibility: EFileVisibility;
}

/**
 * Client side directory model.
 */
export interface IDirectory {
  id: number;
  created_at: string;
  updated_at: string;
  uuid: string;
  path: string;
  visibility: EFileVisibility;
}

/**
 * Notification (snackbar) interface.
 */
export interface INotification {
  uuid?: string;
  message: string;
  severity: AlertColor;
  open?: boolean;
  timestamp?: string;
  duration?: number;
}
