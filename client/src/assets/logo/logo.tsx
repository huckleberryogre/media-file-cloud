interface IProps extends React.SVGProps<SVGSVGElement> {
  circleColor?: string;
  triangleColor?: string;
  borderWidth?: number;
  borderColor?: string;
}

export const LogoSVG = ({
  circleColor = '#FFED00',
  triangleColor = '#72FF13',
  borderWidth = 0,
  borderColor = undefined,
  ...props
}: IProps) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    version="1.0"
    width="512pt"
    height="512pt"
    viewBox="0 0 512 512"
    preserveAspectRatio="xMidYMid meet"
    {...props}
  >
    <circle
      cx="256"
      cy="256"
      r="234"
      stroke={circleColor}
      strokeWidth="30"
      fill="none"
    />
    {borderWidth > 0 && (
      <>
        <circle
          cx="256"
          cy="256"
          r="219"
          stroke={borderColor}
          strokeWidth={borderWidth}
          fill="none"
        />
        <circle
          cx="256"
          cy="256"
          r="249"
          stroke={borderColor}
          strokeWidth={borderWidth}
          fill="none"
        />
      </>
    )}
    <path
      fill={triangleColor}
      stroke={borderColor}
      strokeWidth={borderWidth}
      d=" M303.783936,197.212372   C318.827911,206.803940 333.300232,216.658890 348.396484,225.441238   C363.805145,234.405289 369.224640,261.356628 348.690826,274.993408   C333.586670,285.024200 317.729156,293.918182 302.238739,303.370239   C272.958771,321.236511 243.729874,339.186707 214.417755,357.000031   C205.215988,362.592072 196.251144,368.623077 185.104385,370.174744   C166.297668,372.792725 144.809036,361.548676 137.814133,343.965454   C135.053253,337.025421 133.267334,329.202393 133.206467,321.763672   C132.806885,272.939667 132.991837,224.110794 133.004181,175.283508   C133.010239,151.327499 146.148102,133.503250 169.522507,128.104248   C181.821381,125.263451 193.828705,128.629547 204.712311,135.450119   C235.327789,154.636337 266.019135,173.701477 296.682312,192.811600   C298.943237,194.220642 301.217865,195.607681 303.783936,197.212372  z"
    />
  </svg>
);
