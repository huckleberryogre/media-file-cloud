import { test } from '@playwright/test';

/**
 * Happy path test suit.
 */
test('happy path test', async ({ page }) => {
  await page.goto('http://localhost:3000');
  const bodyLocator = await page.locator('body');

  await bodyLocator.elementHandle({ timeout: 5000 });
  await page.waitForSelector('#root', { timeout: 5000 });
});
