# Media file cloud client

- React + Redux Toolkit + Typescript + Material UI.
- webpack dev server run on http://localhost:3000 and configured to proxy API requests to the main server

## Install

```shell
yarn install
```

## Run

```shell
yarn start
```

To run e2e you may need to download browsers drivers:

```shell
npx playwright install
```

## Run unit tests

```shell
yarn run test
```

## Run e2e tests

```
yarn run test:e2e
```
