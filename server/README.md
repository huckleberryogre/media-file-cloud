# Media file cloud server

* NestJS + TypeORM.
* dev server run on http://localhost:4000

## Installation

```shell
yarn install
```

## Troubleshooting

if installation failed with the `node-gyp` package and it says it requires python or xcode-tools, run this command (on mac):
```shell
xcode-select --install
```

see linux/windows and additional instructions here https://github.com/nodejs/node-gyp

If you want to run TypeORM CLI (for migrations) you may need ts-node to be installed globally:

```shell
yarn add global ts-node
```

## Running the app [development]

```shell
yarn run start
```

## Running unit tests

```shell
yarn run test
```
