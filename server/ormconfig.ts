import { DataSourceOptions } from 'typeorm';

const config: DataSourceOptions = {
  type: 'better-sqlite3',
  database: 'database/sqlite3.db',
  migrations: ['database/migrations/*.ts'],
  entities: ['src/**/*.entity.js'],
};

export default config;
