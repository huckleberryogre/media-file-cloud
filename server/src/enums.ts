export enum EUserRole {
  ADMIN = 'ADMIN',
  USER = 'USER',
}

export enum EEntityVisibility {
  PUBLIC = 'PUBLIC',
  PRIVATE = 'PRIVATE',
}
