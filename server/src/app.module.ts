import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { FilesModule } from './modules/files/files.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { TagsModule } from './modules/tags/tags.module';
import { DATABASE_DIRECTORY, SERVE_STATIC_DIRECTORY } from './config';
import { NotificationsModule } from './modules/notifications/notifications.module';

const typeOrmModule = TypeOrmModule.forRoot({
  type: 'better-sqlite3',
  database: `${DATABASE_DIRECTORY}/sqlite3-file-vault.db`,
  synchronize: true,
  migrationsRun: true,
  logging: ['info', 'migration', 'error', 'log', 'schema'],
  entities: ['dist/**/*.entity'],
  autoLoadEntities: true,
  migrations: ['dist/database/migrations/*'],
});

@Module({
  imports: [
    typeOrmModule,
    FilesModule,
    AuthModule,
    UsersModule,
    ServeStaticModule.forRoot({
      rootPath: SERVE_STATIC_DIRECTORY,
    }),
    EventEmitterModule.forRoot(),
    TagsModule,
    NotificationsModule,
  ],
})
export class AppModule {}
