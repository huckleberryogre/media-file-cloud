/**
 * User model returned when google authentication strategy is used.
 */
export interface IGoogleAuthUser {
  email: string;
  firstName: string;
  lastName: string;
  picture: string;
  accessToken: string;
}
