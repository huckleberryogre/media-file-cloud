/**
 * Events for the event emitter.
 */
export const EVENTS = {
  NOTIFICATION: 'notification',
  FILES_ENTRIES_UPDATE: 'filesEntriesUpdate',
  USERS_SERVICE_READY: 'usersServiceReady',
};
