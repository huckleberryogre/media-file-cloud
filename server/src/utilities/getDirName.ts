import { dirname } from 'path';
import { fileURLToPath } from 'url';

/**
 * Returns __dirname
 * See https://stackoverflow.com/a/50052194
 *
 * @param importMetaUrl A "import.meta.url" must be provided from the target file.
 */
export const getDirName = (importMetaUrl: string) =>
  dirname(fileURLToPath(importMetaUrl));
