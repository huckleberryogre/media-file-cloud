import assert from 'assert';

type TAssertEnvironmentVariable = (
  variableName: string,
) => asserts variableName;

/**
 * Asserts environment variable.
 *
 * @param variableName Variable name.
 */
export const assertEnvironmentVariable: TAssertEnvironmentVariable = (
  variableName: string,
): asserts variableName => {
  assert(
    process.env[variableName],
    `An environment variable "${variableName}" is not provided, please check the .env file`,
  );
};
