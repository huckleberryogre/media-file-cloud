import { isAbsolute, join } from 'path';
import os from 'os';
import { mkdirp } from 'mkdirp';
import { assertEnvironmentVariable } from './utilities/assertEnvironmentVariable';

assertEnvironmentVariable('FILES_VAULT_DIRECTORY');
assertEnvironmentVariable('DATABASE_DIRECTORY');

/**
 * Absolute file vault path.
 */
export const FILES_VAULT_DIRECTORY = isAbsolute(
  process.env.FILES_VAULT_DIRECTORY,
)
  ? process.env.FILES_VAULT_DIRECTORY
  : join(os.homedir(), process.env.FILES_VAULT_DIRECTORY);

// create vault directory if it does not exist
mkdirp(FILES_VAULT_DIRECTORY);

/**
 * Absolute database path.
 */
export const DATABASE_DIRECTORY = isAbsolute(process.env.DATABASE_DIRECTORY)
  ? process.env.DATABASE_DIRECTORY
  : join(os.homedir(), process.env.DATABASE_DIRECTORY);

/**
 * Is Google authentication enabled.
 * Returns true if both GOOGLE_CLIENT_ID and GOOGLE_SECRET are provided.
 */
export const isGoogleAuthEnabled = !!(
  process.env.GOOGLE_CLIENT_ID && process.env.GOOGLE_SECRET
);

try {
  assertEnvironmentVariable('GOOGLE_CLIENT_ID');
  assertEnvironmentVariable('GOOGLE_SECRET');
} catch {
  console.warn(
    'Google authentication is disabled. Please provide GOOGLE_CLIENT_ID and GOOGLE_SECRET environment variables to enable the feature.',
  );
}

/**
 * Absolute path to static directory. Used by NestJS ServeStaticModule.
 * @see https://docs.nestjs.com/techniques/static-assets
 */
export const SERVE_STATIC_DIRECTORY = join(__dirname, './client');
