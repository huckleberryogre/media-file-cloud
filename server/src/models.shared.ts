import { ICommonTagsResult, IFormat } from 'music-metadata/lib/type';
import type sharp from 'sharp';
import { FfprobeFormat, FfprobeStream } from 'fluent-ffmpeg';

/**
 * A feedback messages model to be used with the toast messages on the client.
 */
export interface IFeedback {
  info?: string | string[];
  success?: string | string[];
  warning?: string | string[];
  error?: string | string[];
}

/**
 * Audio metadata model to be sent to the client.
 */
export type TAudioMetadataClientModel = {
  format: Pick<IFormat, 'duration' | 'bitrate' | 'lossless'>;
  common: Pick<
    ICommonTagsResult,
    | 'title'
    | 'album'
    | 'artist'
    | 'artists'
    | 'year'
    | 'track'
    | 'genre'
    | 'albumartist'
    | 'disk'
  >;
};

/**
 * Image metadata model to be sent to the client.
 */
export type TImageMetadataClientModel = Pick<
  sharp.Metadata,
  'format' | 'width' | 'height' | 'size'
>;

/**
 * Video metadata model to be sent to the client.
 */
export type TVideoMetadataClientModel = {
  format: Pick<FfprobeFormat, 'size' | 'duration'>;
  streams: Pick<
    FfprobeStream,
    'width' | 'height' | 'index' | 'codec_name' | 'codec_type' | 'duration'
  >[];
};
