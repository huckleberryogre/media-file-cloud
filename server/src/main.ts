import 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { FILES_VAULT_DIRECTORY } from './config';
import { JwtAuthGuard } from './modules/auth/guards/jwt-auth.guard';
import cookieParser from 'cookie-parser';
import session from 'express-session';

const PORT = process.env.PORT;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser());
  app.use(
    session({
      // todo: change secret to env variable and check other options
      secret: 'my-secret',
      resave: false,
      saveUninitialized: true,
    }),
  );

  /**
   * Adds swagger api page.
   */
  const config = new DocumentBuilder()
    .setTitle('API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  /**
   * Adds global guard.
   */
  const jwtAuthGuard = app.get(JwtAuthGuard);
  app.useGlobalGuards(jwtAuthGuard);

  /**
   * Run app.
   */
  await app.listen(PORT);

  console.log(`App is running on http://localhost:${PORT}`);

  console.log(`\nlocal files directory: file://${FILES_VAULT_DIRECTORY}`);
}

bootstrap();
