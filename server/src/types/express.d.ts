import { UserEntity } from '../modules/users/entities/user.entity';

declare global {
  namespace Express {
    export interface User extends UserEntity {}
  }
}
