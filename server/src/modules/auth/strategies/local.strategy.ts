import {
  IStrategyOptions,
  IVerifyOptions,
  Strategy,
  VerifyFunction,
} from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';

/**
 *  ❗ Strategy is about how to authenticate a user, while a Guard is
 *  about whether to allow an authenticated user to access a specific route.
 *
 *  Call super.canActivate to execute the strategy's validate method.
 */
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'email',
      passwordField: 'password',
    } as IStrategyOptions);
  }

  /**
   * Validate function here is required by the design of Passport Strategy for nestjs.
   *
   * should return user entity (that will be passed further with the request object)
   */
  validate: VerifyFunction = async (
    email: string,
    password: string,
    done: (
      error: any,
      user?: Express.User | false,
      options?: IVerifyOptions,
    ) => void,
  ) => {
    const user = await this.authService.handleLocalUser(email, password);

    if (!user) {
      return done(new UnauthorizedException(), false);
    }

    return done(null, user);
  };
}
