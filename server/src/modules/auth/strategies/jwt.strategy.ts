import {
  ExtractJwt,
  Strategy,
  StrategyOptions,
  VerifiedCallback,
  VerifyCallback,
} from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserEntity } from '../../users/entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

/**
 *  ❗ Strategy is about how to authenticate a user, while a Guard is
 *  about whether to allow an authenticated user to access a specific route.
 *
 *  Call super.canActivate to execute the strategy's validate method.
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_AUTH_SECRET,
    } as StrategyOptions);
  }

  validate: VerifyCallback = async (
    user: UserEntity,
    done: VerifiedCallback,
  ) => {
    user = await this.userRepository.findOne({ where: { email: user.email } });

    if (!user) {
      return done(new UnauthorizedException(), false);
    } else {
      return done(null, user);
    }
  };
}
