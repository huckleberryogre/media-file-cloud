import { BaseEntity } from '../../../enitites/base.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class RegistrationInviteEntity extends BaseEntity {
  /**
   * UUID for the registration invite.
   */
  @Column({ unique: true })
  uuid: string;

  /**
   * Expiration date for the registration invite.
   */
  @Column()
  expiresAt: Date;
}
