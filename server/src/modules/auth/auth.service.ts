import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'bcrypt';
import { instanceToPlain } from 'class-transformer';
import { DeepPartial, Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { IGoogleAuthUser } from '../../models';
import { UserEntity } from '../users/entities/user.entity';
import { EGoogleLoginUserStatus, ERegisterUserStatus } from './enums';
import { RegistrationInviteEntity } from './entities/registration-invite.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { randomUUID } from 'crypto';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private readonly usersService: UsersService,
    private jwtService: JwtService,
    @InjectRepository(RegistrationInviteEntity)
    private readonly registrationInviteRepository: Repository<RegistrationInviteEntity>,
  ) {}

  /**
   * Creates invite for registration.
   */
  async createRegistrationInvite(): Promise<RegistrationInviteEntity> {
    const entityLike: DeepPartial<RegistrationInviteEntity> = {
      uuid: randomUUID(),
      expiresAt: new Date(Date.now() + 31 * 24 * 60 * 60 * 1000),
    };

    const entity = this.registrationInviteRepository.create(entityLike);

    return this.registrationInviteRepository.save(entity);
  }

  /**
   * Function used in standard passport "local" strategy.
   *
   * Should return user if given credentials are valid or falsy value otherwise.
   */
  async handleLocalUser(email: string, password: string): Promise<UserEntity> {
    const user = await this.usersService.findOne(
      { email: email },
      {
        select: ['password'],
      },
    );
    let isValid = false;

    if (user) {
      isValid = await compare(password, user.password);
    }

    return isValid ? await this.usersService.findOne({ email }) : null;
  }

  /**
   * Returns user information within jwt token.
   */
  login(user: UserEntity): string {
    delete user.id;
    return this.jwtService.sign(instanceToPlain(user));
  }

  /**
   * Register user if not exists.
   */
  async createUser(createUserDto: DeepPartial<UserEntity>, isMaster = false) {
    let registerStatus: ERegisterUserStatus;
    let user: UserEntity;
    const isUserExists = Boolean(
      await this.usersService.usersCount({
        email: createUserDto.email,
      }),
    );

    if (isUserExists) {
      registerStatus = ERegisterUserStatus.EXISTS;
    } else {
      try {
        user = await this.usersService.createAdminUser(createUserDto, isMaster);
        registerStatus = ERegisterUserStatus.CREATED;
      } catch (error) {
        this.logger.error(error);
        registerStatus = ERegisterUserStatus.ERROR;
      }
    }

    return {
      user,
      status: registerStatus,
    };
  }

  /**
   * Registers new user.
   *
   * Returns jwt token containing new user.
   */
  async register(
    createUserDto: DeepPartial<UserEntity>,
    invitationUUID?: string,
  ): Promise<{ jwt: string; user: UserEntity; status: ERegisterUserStatus }> {
    let registerStatus: ERegisterUserStatus;
    let user: UserEntity;
    const hasMasterUser = Boolean(
      await this.usersService.usersCount({
        isMaster: true,
      }),
    );

    if (hasMasterUser) {
      if (invitationUUID) {
        const invite = await this.registrationInviteRepository.findOne({
          where: {
            uuid: invitationUUID,
          },
        });

        if (invite && invite.expiresAt > new Date()) {
          const result = await this.createUser(createUserDto, false);
          user = result.user;
          registerStatus = result.status;

          if (registerStatus === ERegisterUserStatus.CREATED) {
            await this.registrationInviteRepository.delete(invite.id);
          }
        } else {
          registerStatus = ERegisterUserStatus.INVALID_INVITE_LINK;
        }
      } else {
        registerStatus = ERegisterUserStatus.INVALID_INVITE_LINK;
      }
    } else {
      const result = await this.createUser(createUserDto, true);
      user = result.user;
      registerStatus = result.status;
    }

    return {
      jwt:
        registerStatus === ERegisterUserStatus.CREATED
          ? this.login(user)
          : null,
      user,
      status: registerStatus,
    };
  }

  /**
   * Function used in google auth2 strategy.
   *
   * Should return access token with found user.
   */
  async handleGoogleUser(user: IGoogleAuthUser): Promise<{
    jwt: string;
    user: UserEntity;
    status: EGoogleLoginUserStatus;
  }> {
    let status: EGoogleLoginUserStatus;
    let userEntity: UserEntity;
    let jwt: string;

    if (!user) {
      status = EGoogleLoginUserStatus.NOT_FOUND;
    } else {
      const existingUser = await this.usersService.findOne({
        email: user.email,
      });
      const userExists = Boolean(existingUser);

      if (userExists) {
        status = EGoogleLoginUserStatus.SUCCESS;
        userEntity = existingUser;
        jwt = this.login(userEntity);
      } else {
        const {
          jwt: registerJwt,
          user: registerUser,
          status: registerStatus,
        } = await this.register(user);

        if (registerStatus === ERegisterUserStatus.CREATED) {
          status = EGoogleLoginUserStatus.CREATED;
          userEntity = registerUser;
          jwt = registerJwt;
        } else {
          status =
            registerStatus === ERegisterUserStatus.ADMIN_ALREADY_EXISTS
              ? EGoogleLoginUserStatus.ADMIN_ALREADY_EXISTS
              : EGoogleLoginUserStatus.ERROR;
        }
      }
    }

    return {
      jwt,
      user: userEntity,
      status,
    };
  }
}
