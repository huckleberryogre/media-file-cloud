import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { GoogleStrategy } from './strategies/google.strategy';
import { AuthController } from './auth.controller';
import { isGoogleAuthEnabled } from '../../config';
import { RegistrationInviteEntity } from './entities/registration-invite.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../users/entities/user.entity';
import { FileEntity } from '../files/entities/file.entity';

const providers = [AuthService, LocalStrategy, JwtStrategy];

if (isGoogleAuthEnabled) {
  providers.push(GoogleStrategy as any);
}

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({ secret: process.env.JWT_AUTH_SECRET }),
    TypeOrmModule.forFeature([
      RegistrationInviteEntity,
      UserEntity,
      FileEntity,
    ]),
  ],
  providers,
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
