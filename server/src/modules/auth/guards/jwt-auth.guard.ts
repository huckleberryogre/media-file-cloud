import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { IS_PUBLIC_KEY } from '../decorators/public.decorator';
import { EEntityVisibility } from '../../../enums';
import { FileEntity } from '../../files/entities/file.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';

/**
 *  ❗ Strategy is about how to authenticate a user, while a Guard is
 *  about whether to allow an authenticated user to access a specific route.
 *
 *  Call super.canActivate to execute the strategy's validate method.
 */
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(
    private readonly reflector: Reflector,
    @InjectRepository(FileEntity)
    private readonly fileRepository: Repository<FileEntity>,
    private readonly jwtService: JwtService,
  ) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) {
      // 💡 See this condition
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const uuid: string = request.params.uuid;

    if (request.route.path === '/api/files/:uuid' && uuid) {
      const file = await this.fileRepository.findOne({
        where: { uuid },
        relations: ['owner'],
      });
      const owner = file?.owner;

      if (file) {
        if (file.visibility === EEntityVisibility.PUBLIC) {
          return true;
        } else {
          const token = request.cookies.token;
          const userFromToken = this.jwtService.decode(token);

          if (userFromToken && owner?.email !== userFromToken['email']) {
            return false;
          }
        }
      }
    }

    // if authorization cookie is not provided but token cookie is there
    // then provide authorization cookie from token cookie
    // this hack is needed for the case when e.g. image is loaded from within
    // the <img> tag and request is sent without authorization header
    if (!request.headers.authorization && request.cookies.token) {
      const token = request.cookies.token;
      request.headers.authorization = `Bearer ${token}`;
    }

    await super.canActivate(context);

    return true;
  }
}
