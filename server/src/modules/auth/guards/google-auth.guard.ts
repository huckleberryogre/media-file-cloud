import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

/**
 *  ❗ Strategy is about how to authenticate a user, while a Guard is
 *  about whether to allow an authenticated user to access a specific route.
 *
 *  Call super.canActivate to execute the strategy's validate method.
 */
@Injectable()
export class GoogleAuthGuard extends AuthGuard('google') {}
