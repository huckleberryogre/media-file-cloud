import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  HttpCode,
  InternalServerErrorException,
  Logger,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { GoogleAuthGuard } from './guards/google-auth.guard';
import type { Request, Response } from 'express';
import { IGoogleAuthUser } from '../../models';
import { Public } from './decorators/public.decorator';
import { EGoogleLoginUserStatus, ERegisterUserStatus } from './enums';
import { IFeedback } from '../../models.shared';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { FEEDBACK_TEXTS } from './feedback.texts';

@Controller('api/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  private readonly logger = new Logger(AuthController.name);

  /**
   * Creates invite for registration.
   */
  @Post('create-registration-invite')
  async createRegistrationInvite(@Res() res: Response, @Req() req: Request) {
    let feedback: IFeedback = {
      error: FEEDBACK_TEXTS.createRegistrationInvite.error.generic,
    };

    if (!req.user.isMaster) {
      feedback = {
        error: FEEDBACK_TEXTS.createRegistrationInvite.error.onlyMaster,
      };

      throw new ForbiddenException({ feedback });
    }

    try {
      const invite = await this.authService.createRegistrationInvite();
      feedback = {
        success: FEEDBACK_TEXTS.createRegistrationInvite.success,
      };

      return res.send({ uuid: invite.uuid, feedback });
    } catch (error) {
      this.logger.error(error);

      throw new InternalServerErrorException({ feedback });
    }
  }

  /**
   * Utility function sets jwt token cookie on response object.
   */
  private static setResponseCookieToken(res: Response, jwt: string) {
    const expires = new Date();
    expires.setTime(expires.getTime() + 24 * 60 * 60 * 1000);

    // for cookie options see https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies
    res.cookie('token', jwt, {
      sameSite: true,
      expires,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Req() req: Request) {
    return req.user;
  }

  @Public()
  @UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @Post('login')
  async login(@Req() req: Request, @Res({ passthrough: true }) res: Response) {
    const jwt = this.authService.login(req.user);

    AuthController.setResponseCookieToken(res, jwt);

    return {
      feedback: {
        success: FEEDBACK_TEXTS.login.success,
      },
    };
  }

  @Public()
  @Post('register')
  async register(
    @Query('invitationUUID') invitationUUID: string,
    @Body() createUserDto: CreateUserDto,
    @Res() res: Response,
  ) {
    let feedback: IFeedback;
    const { jwt, user, status } = await this.authService.register(
      createUserDto,
      invitationUUID,
    );
    const message = FEEDBACK_TEXTS.getRegisterFeedbackMessage(user?.role)[
      status
    ];

    switch (status) {
      case ERegisterUserStatus.ADMIN_ALREADY_EXISTS:
        feedback = { warning: message };
        res.status(403);
        break;
      case ERegisterUserStatus.CREATED:
        // If admin is created successfully, log him in.
        // todo: Replace with email verification.
        AuthController.setResponseCookieToken(res, jwt);
        feedback = { success: message };
        break;
      case ERegisterUserStatus.EXISTS:
        feedback = { warning: message };
        res.status(403);
        break;
      case ERegisterUserStatus.ERROR:
        feedback = { error: message };
        res.status(500);
        break;
      case ERegisterUserStatus.INVALID_INVITE_LINK:
        feedback = { error: message };
        res.status(404);
        break;
      default:
        feedback = {
          error: message || FEEDBACK_TEXTS.register.unexpectedError,
        };
        res.status(500);
    }

    return res.send({ feedback });
  }

  @Public()
  @Get('google')
  @UseGuards(GoogleAuthGuard)
  async googleAuth() {
    // starts google authentication dance
  }

  @Public()
  @Get('google/handle-google-auth')
  async handleGoogleAuth() {
    return `
      <!DOCTYPE html>
      <html>
        <head>
          <script>
            window.opener.postMessage(
              {
                isLoginPopup: true,
              },
              window.location.origin
            );
            window.close();
          </script>
        </head>
        <body></body>
      </html>
    `;
  }

  @Public()
  @Get('google/redirect')
  @UseGuards(GoogleAuthGuard)
  async googleAuthRedirect(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<void> {
    let feedback: IFeedback;
    const { jwt, status, user } = await this.authService.handleGoogleUser(
      req.user as any as IGoogleAuthUser,
    );
    const message = FEEDBACK_TEXTS.getGoogleLoginFeedbackMessage(user?.role)[
      status
    ];

    if (jwt) {
      AuthController.setResponseCookieToken(res, jwt);
    }

    switch (status) {
      case EGoogleLoginUserStatus.SUCCESS:
      case EGoogleLoginUserStatus.CREATED:
        feedback = { success: message };
        break;
      case EGoogleLoginUserStatus.EXISTS:
      case EGoogleLoginUserStatus.ADMIN_ALREADY_EXISTS:
        feedback = { warning: message };
        res.status(403);
        break;
      case EGoogleLoginUserStatus.ERROR:
        feedback = { error: message };
        res.status(500);
        break;
      case EGoogleLoginUserStatus.NOT_FOUND:
        feedback = { error: message };
        res.status(404);
        break;
    }

    res.cookie('feedback', JSON.stringify(feedback));

    return res.redirect(`handle-google-auth`);
  }
}
