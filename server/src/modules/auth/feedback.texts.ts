import { EUserRole } from '../../enums';
import { EGoogleLoginUserStatus, ERegisterUserStatus } from './enums';

export const FEEDBACK_TEXTS = {
  /**
   * General feedback messages.
   */
  login: {
    success: 'Welcome aboard captain!',
  },
  register: {
    inviteRequired: 'Ask administrator for an invite link',
    unexpectedError: 'Unexpected error',
  },
  /**
   * Feedback messages for google login endpoint.
   */
  getGoogleLoginFeedbackMessage: (role?: EUserRole) => ({
    [EGoogleLoginUserStatus.ADMIN_ALREADY_EXISTS]: `Admin already exists`,
    [EGoogleLoginUserStatus.EXISTS]: `${
      role?.toLowerCase() || 'user'
    } with this email already exists`,
    [EGoogleLoginUserStatus.CREATED]: `${
      role?.toLowerCase() || 'user'
    } created successfully`,
    [EGoogleLoginUserStatus.ERROR]: `Failed to create ${
      role?.toLowerCase() || 'user'
    }`,
    [EGoogleLoginUserStatus.SUCCESS]: FEEDBACK_TEXTS.login.success,
  }),
  /**
   * Feedback messages for register endpoint.
   */
  getRegisterFeedbackMessage: (role?: EUserRole) => ({
    [ERegisterUserStatus.ADMIN_ALREADY_EXISTS]: `Admin already exists`,
    [ERegisterUserStatus.EXISTS]: `${
      role?.toLowerCase() || 'user'
    } with this email already exists`,
    [ERegisterUserStatus.CREATED]: `${
      role?.toLowerCase() || 'user'
    } created successfully`,
    [ERegisterUserStatus.ERROR]: `Failed to create ${
      role?.toLowerCase() || 'user'
    }`,
    [ERegisterUserStatus.INVALID_INVITE_LINK]: `Invite link is invalid or expired`,
  }),
  createRegistrationInvite: {
    success: 'Registration link created',
    error: {
      generic: 'Failed to create registration link',
      onlyMaster: 'Only master user can create registration link',
    },
  },
};
