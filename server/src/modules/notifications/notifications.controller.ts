import { Controller, Injectable, Logger, Req, Sse } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Observable, Subscriber } from 'rxjs';
import { IFeedback } from '../../models.shared';
import { EVENTS } from '../../constants';
import { Public } from '../auth/decorators/public.decorator';
import type { Request } from 'express';

@Injectable()
@Controller('api/notifications')
export class NotificationsController {
  constructor(private eventEmitter: EventEmitter2) {}

  private readonly logger = new Logger(NotificationsController.name);
  private observables: Record<string, Observable<any>> = {};

  @Public()
  @Sse('feedback')
  sendNotifications(@Req() req: Request): Observable<IFeedback> {
    const handleNotification = (
      subscriber: Subscriber<any>,
      feedback: IFeedback,
    ) => {
      subscriber.next({ data: feedback });
    };

    if (!this.observables[req.sessionID]) {
      this.observables[req.sessionID] = new Observable((subscriber) => {
        const listener = (feedback: IFeedback) =>
          handleNotification(subscriber, feedback);
        this.eventEmitter.on(EVENTS.NOTIFICATION, listener);

        return () => {
          this.eventEmitter.off(EVENTS.NOTIFICATION, listener);
        };
      });
    }

    return this.observables[req.sessionID];
  }
}
