import { Controller, Get } from '@nestjs/common';
import { isGoogleAuthEnabled } from '../../config';
import { Public } from '../auth/decorators/public.decorator';
import { UsersService } from './users.service';
import { EUserRole } from '../../enums';

@Controller('api/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Public()
  @Get('features')
  async getFeatures() {
    const hasMasterUser = Boolean(
      await this.usersService.usersCount({
        role: EUserRole.ADMIN,
      }),
    );
    return {
      googleAuth: isGoogleAuthEnabled,
      hasMasterUser,
    };
  }
}
