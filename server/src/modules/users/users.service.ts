import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import {
  DeepPartial,
  FindOneOptions,
  FindOptionsWhere,
  Repository,
} from 'typeorm';
import { EUserRole } from '../../enums';
import chalk from 'chalk';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { EVENTS } from '../../constants';

@Injectable()
export class UsersService implements OnModuleInit {
  private readonly logger = new Logger(UsersService.name);

  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  async onModuleInit() {
    const masterUserEntity = {
      email: process.env.MASTER_USER_EMAIL,
      password: process.env.MASTER_USER_PASSWORD,
      username: process.env.MASTER_USER_EMAIL?.split('@')[0],
    };

    const masterUser = await this.findOne({ isMaster: true });

    if (!masterUser) {
      try {
        this.logger.log(chalk.blue('Master user not found. Creating...'));
        await this.createAdminUser(masterUserEntity, true);
      } catch (error) {
        this.logger.error(
          'Error while creating master user on module init. Provide valid MASTER_USER_EMAIL and MASTER_USER_PASSWORD env variables or register using web interface.',
        );
        this.logger.error(chalk.red(error));
      }
    } else {
      this.logger.log(chalk.blue('Master user already exists.'));
    }

    this.eventEmitter.emit(EVENTS.USERS_SERVICE_READY);
  }

  /**
   * Creates master user.
   */
  async createAdminUser(
    entityLike: DeepPartial<UserEntity>,
    isMaster = false,
  ): Promise<UserEntity> {
    entityLike.role = EUserRole.ADMIN;
    entityLike.isMaster = isMaster;

    return this.createAndSaveUser(entityLike);
  }

  async createAndSaveUser(
    entityLike: DeepPartial<UserEntity>,
  ): Promise<UserEntity> {
    let userEntity: UserEntity;

    try {
      // If username is not provided, use email without domain part.
      if (!entityLike.username) {
        entityLike.username = entityLike.email?.split('@')[0];
      }

      userEntity = this.usersRepository.create(entityLike);
      await this.usersRepository.save(userEntity);
      this.logger.log(
        chalk.blue(`${userEntity.role} created: ${userEntity.email}`),
      );
    } catch (error) {
      this.logger.error(
        `Error while creating ${entityLike.role} ${entityLike.email}`,
      );
      this.logger.error(chalk.red(error));
    }

    return userEntity;
  }

  findOne(
    conditions: FindOptionsWhere<UserEntity>,
    options?: FindOneOptions<UserEntity>,
  ): Promise<UserEntity> {
    return this.usersRepository.findOne({ where: conditions, ...options });
  }

  usersCount(entityLike?: FindOptionsWhere<UserEntity>): Promise<number> {
    return this.usersRepository.count({ where: entityLike });
  }
}
