import { BeforeInsert, BeforeUpdate, Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../enitites/base.entity';
import { EUserRole } from '../../../enums';
import { hash } from 'bcrypt';
import { FileEntity } from '../../files/entities/file.entity';
import { Exclude } from 'class-transformer';
import { TagEntity } from '../../tags/entities/tag.entity';
import { DirectoryEntity } from '../../files/entities/directory.entity';

@Entity()
export class UserEntity extends BaseEntity {
  /**
   * User email.
   */
  @Column({ unique: true })
  email: string;

  /**
   * User password (hashed).
   */
  @Column({ select: false, nullable: true })
  password: string;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (this.password) {
      this.password = await hash(this.password, 10);
    }
  }

  /**
   * User name.
   */
  @Column({ nullable: true })
  username?: string;

  /**
   * User role.
   */
  @Column()
  role: EUserRole;

  /**
   * URL to the avatar.
   */
  @Column({ nullable: true })
  avatar?: string;

  /**
   * The access token granted to the user
   * to use in google API requests.
   */
  @Column({ nullable: true, select: false })
  googleAccessToken?: string;

  /**
   * Master user flag.
   */
  @Column({ default: false })
  isMaster: boolean;

  /**
   * User files.
   */
  @OneToMany(() => FileEntity, (file) => file.owner)
  @Exclude()
  files: FileEntity[];

  /**
   * User directories.
   */
  @OneToMany(() => DirectoryEntity, (directory) => directory.owner)
  @Exclude()
  directories: DirectoryEntity[];

  /**
   * User tags.
   */
  @OneToMany(() => TagEntity, (tag) => tag.user)
  tags: TagEntity[];
}
