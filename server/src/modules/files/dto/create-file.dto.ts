/**
 * Create file DTO.
 */
export class CreateFileDto {
  uuid: string;
  name: string;
  hash: string;
  metadata: string;
}
