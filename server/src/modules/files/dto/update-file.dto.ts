import { PartialType } from '@nestjs/mapped-types';
import { CreateFileDto } from './create-file.dto';
import { EEntityVisibility } from '../../../enums';

export class UpdateFileDto extends PartialType(CreateFileDto) {
  visibility: EEntityVisibility;
}
