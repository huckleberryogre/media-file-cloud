import { EEntityVisibility } from '../../enums';

export const FEEDBACK_TEXTS = {
  upload: {
    success: 'Files uploaded',
    error: {
      generic: 'Failed to upload files',
    },
  },
  file: {
    visibility: {
      success: (visibility: EEntityVisibility) =>
        `File visibility set to ${visibility?.toLowerCase()}`,
      error: {
        generic: 'Failed to change file visibility',
      },
    },
  },
};
