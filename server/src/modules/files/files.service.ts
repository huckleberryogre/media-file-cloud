import { ForbiddenException, Injectable, Logger } from '@nestjs/common';
import { UpdateFileDto } from './dto/update-file.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { FileEntity } from './entities/file.entity';
import { FindOneOptions, FindOptionsWhere, Like, Repository } from 'typeorm';
import { createHash, randomUUID } from 'crypto';
import { readFile, writeFile } from 'fs/promises';
import { mkdirp } from 'mkdirp';
import path, { relative } from 'path';
import { watch } from 'chokidar';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { unlink } from 'fs';
import { promisify } from 'util';
import type { Express } from 'express';
import {
  IFeedback,
  TAudioMetadataClientModel,
  TImageMetadataClientModel,
  TVideoMetadataClientModel,
} from '../../models.shared';
import { DATABASE_DIRECTORY, FILES_VAULT_DIRECTORY } from '../../config';
import { isNotHiddenFile } from './utilities/isNotHiddenFile';
import { scheduler } from 'timers/promises';
import { UserEntity } from '../users/entities/user.entity';
import { fromBuffer } from 'file-type';
import { instanceToPlain } from 'class-transformer';
import { parseBuffer } from 'music-metadata';
import sharp from 'sharp';
import ffmpeg, { FfprobeData } from 'fluent-ffmpeg';
import { EVENTS } from '../../constants';
import { IAudioMetadata } from 'music-metadata/lib/type';
import chalk from 'chalk';
import { DirectoryEntity } from './entities/directory.entity';
import { getOwner } from './utilities/getOwner';
import { getFilePath } from './utilities/getFilePath';
import { EEntityVisibility } from '../../enums';

const ffprobeAsync = promisify<string, FfprobeData>(ffmpeg.ffprobe);

const unlinkPromise = promisify(unlink);

interface IFileStatistics {
  added: FileEntity[];
}

interface IUserFilesStatistics {
  [userName: string]: IFileStatistics;
}

@Injectable()
export class FilesService {
  isWatchingFilesDir = true;
  private initialScanComplete = false;
  private initialScanStatistics: IUserFilesStatistics = {};
  private readonly logger = new Logger(FilesService.name);

  constructor(
    @InjectRepository(FileEntity)
    private readonly fileRepository: Repository<FileEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(DirectoryEntity)
    private readonly directoryRepository: Repository<DirectoryEntity>,
    private eventEmitter: EventEmitter2,
  ) {
    this.eventEmitter.on(EVENTS.USERS_SERVICE_READY, this.watchFilesDirectory);
  }

  /**
   * Handles files upload endpoint.
   */
  public async handleUploadedFiles(
    files: Express.Multer.File[],
    user: UserEntity,
  ) {
    this.setIsWatchingFilesDir(false);

    try {
      const promises = [];
      // filter out hidden files
      const filteredFiles = files.filter((file) =>
        isNotHiddenFile({
          name: file.originalname,
          path: file.fieldname,
        } as FileEntity),
      );

      for (const newFile of filteredFiles) {
        promises.push(this.saveFileOnDisk(newFile, user));
      }

      const filePaths = (await Promise.allSettled<string>(promises))
        .map((result: PromiseFulfilledResult<string>) => result.value)
        .filter((path) => typeof path === 'string');

      await Promise.allSettled(
        filePaths.map((absoluteFilePath) =>
          this.findOrCreateFileEntity(absoluteFilePath, user),
        ),
      );
    } catch (e) {
      this.logger.error('failed uploading files: ', e);
    }

    this.setIsWatchingFilesDir(true);

    this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
  }

  /**
   * Returns all files entries.
   */
  public async findAll(user: UserEntity) {
    const userEntity = await this.userRepository.findOne({
      where: {
        email: user.email,
      },
      select: ['files'],
      relations: ['files'],
    });

    return userEntity.files
      .filter(isNotHiddenFile)
      .map((file) => instanceToPlain(file));
  }

  /**
   * Returns all shared files entries.
   * Checks if uuid is a file or directory.
   * If directory, returns all files inside it recursively.
   */
  public async findSharedFilesEntries(uuid: string) {
    const fileEntry = await this.fileRepository.findOne({
      where: {
        uuid,
      },
    });

    // If it's file entry
    if (fileEntry) {
      if (
        fileEntry.visibility === EEntityVisibility.PUBLIC &&
        isNotHiddenFile(fileEntry)
      ) {
        return [instanceToPlain(fileEntry)];
      } else if (fileEntry.visibility === EEntityVisibility.PRIVATE) {
        throw new ForbiddenException({
          feedback: {
            error: 'Requested resource is private',
          },
        });
      }
    }

    const directoryEntry = await this.directoryRepository.findOne({
      where: {
        uuid,
      },
    });

    // If it's directory entry
    if (directoryEntry) {
      if (directoryEntry?.visibility === EEntityVisibility.PUBLIC) {
        const filesEntries = await this.fileRepository.find({
          where: {
            path: Like(`${directoryEntry.path}%`),
          },
        });

        return filesEntries
          .filter(isNotHiddenFile)
          .map((file) => instanceToPlain(file));
      } else if (directoryEntry?.visibility === EEntityVisibility.PRIVATE) {
        throw new ForbiddenException({
          feedback: {
            error: 'Requested resource is private',
          },
        });
      }
    }

    return [];
  }

  /**
   * Returns all directories.
   */
  public async findDirectories(user: UserEntity) {
    const userEntity = await this.userRepository.findOne({
      where: {
        email: user.email,
      },
      select: ['directories'],
      relations: ['directories'],
    });

    return userEntity.directories.map((directory) =>
      instanceToPlain(directory),
    );
  }

  /**
   * Returns file entry by uuid.
   */
  public findOneFileEntry(uuid: string): Promise<FileEntity> {
    return this.fileRepository.findOne({
      where: {
        uuid,
      },
    });
  }

  /**
   * Updates file with given id.
   */
  public async update(
    uuid: string,
    user: UserEntity,
    updateFileDto: UpdateFileDto,
  ) {
    await this.fileRepository.update(
      {
        uuid,
        owner: user as any as FindOptionsWhere<UserEntity>,
      },
      updateFileDto,
    );
    this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
  }

  /**
   * Removes file entry.
   */
  public async removeFileEntry(conditions: FindOptionsWhere<FileEntity>) {
    const fileEntry = await this.fileRepository.findOne({
      where: conditions,
    });
    const owner = fileEntry?.owner;

    await this.fileRepository.delete(conditions);

    return owner;
  }

  public async unlinkFile(conditions: FindOneOptions<FileEntity>) {
    const file = await this.fileRepository.findOne(conditions);
    const feedback: IFeedback = {};

    if (file) {
      try {
        await unlinkPromise(path.join(FILES_VAULT_DIRECTORY, file.path));
      } catch (e) {
        await this.removeFileEntry(
          conditions.where as FindOptionsWhere<FileEntity>,
        );
        feedback.warning = 'File not found. Database record removed.';
        this.logger.error('File not found, removing file entity ', e);
      }

      if (!feedback.warning) {
        feedback.success = 'File deleted';
      }
    } else {
      feedback.error = 'No such file was found in the records';
    }

    this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);

    return feedback;
  }

  /**
   * Recalculates files metadata.
   */
  public recalculateFilesMetadata = async (user: UserEntity) => {
    const files = await this.fileRepository.find({
      where: {
        owner: {
          id: user.id,
        },
      },
    });

    for (const file of files) {
      try {
        const absoluteFilePath = path.join(FILES_VAULT_DIRECTORY, file.path);
        const fileBuffer = await readFile(absoluteFilePath);
        const { mime } = (await fromBuffer(fileBuffer)) || {};
        const { metadata, metadataFull } = await this.getFileMetadata(
          fileBuffer,
          absoluteFilePath,
          mime,
        );

        await this.fileRepository.update(
          {
            uuid: file.uuid,
          },
          {
            metadata,
            metadataFull,
            mimeType: mime,
          },
        );
      } catch (e) {
        this.logger.error('error recalculating file metadata: ', e, file);
      }
    }
    this.eventEmitter.emit(EVENTS.NOTIFICATION, {
      success: `Metadata recalculated`,
    } as IFeedback);
  };

  private watchFilesDirectory = () => {
    watch(FILES_VAULT_DIRECTORY, {
      // app's database directory is hidden to prevent accidents
      ignored: path.join(DATABASE_DIRECTORY, '**'),
    })
      .on('all', async (event, path) => {
        if (this.isWatchingFilesDir) {
          switch (event) {
            case 'add':
              await this.findOrCreateFileEntity(path);
              this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
              break;
            case 'addDir':
              await this.findOrCreateDirectoryEntity(path);
              this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
              break;
            case 'unlink':
              await this.removeFileEntry({
                path: relative(FILES_VAULT_DIRECTORY, path),
              });
              this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
              break;
            case 'change':
              await this.handleFileChange(path);
              this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
              break;
          }
        }
      })
      // todo: chokidar has ton of unresolved issues and looks like it's not maintained anymore.
      //  Consider switching to watchman https://facebook.github.io/watchman/docs/nodejs or something else.
      //  Ready event being fired before initial scan is complete is one of the issues.
      .on('ready', () => {
        this.initialScanComplete = true;
        this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
        this.logStatistics();
      });
  };

  private logStatistics = () => {
    const newFilesCount = Object.values(this.initialScanStatistics).reduce(
      (sum, { added }) => sum + added.length,
      0,
    );

    this.logger.log(
      chalk.green(
        `Initial scan complete. Recorded ${chalk.yellow(
          newFilesCount,
        )} new files.`,
      ),
    );

    Object.entries(this.initialScanStatistics).forEach(
      ([userEmail, { added }]) => {
        this.logger.log(
          chalk.green(`For ${chalk.blue(userEmail)}. Added new files:`),
        );
        added.forEach((file) => this.logger.log(file.name));
      },
    );
  };

  private setIsWatchingFilesDir = (isWatchingFilesDir: boolean) => {
    this.isWatchingFilesDir = isWatchingFilesDir;
    this.logger.debug(`isWatchingFilesDir: ${this.isWatchingFilesDir}`);
  };

  /**
   * File change handler.
   */
  private handleFileChange = async (absoluteFilePath: string) => {
    const fileEntry = await this.fileRepository.findOneBy({
      path: relative(FILES_VAULT_DIRECTORY, absoluteFilePath),
    });

    if (fileEntry) {
      await this.updateFileEntry(fileEntry);
    }
  };

  /**
   * Updates file entry with actual file from given path.
   */
  private updateFileEntry = async (fileEntry: FileEntity) => {
    const { path: relativeFilePath, uuid } = fileEntry;
    const absoluteFilePath = path.join(FILES_VAULT_DIRECTORY, relativeFilePath);

    try {
      const fileBuffer = await readFile(absoluteFilePath);
      const { mime, ext } = (await fromBuffer(fileBuffer)) || {};

      return await this.fileRepository.update(
        { uuid },
        {
          name: path.basename(absoluteFilePath),
          hash: createHash('sha256').update(fileBuffer).digest('hex'),
          size: Buffer.byteLength(fileBuffer),
          mimeType: mime,
          extension: ext || path.extname(absoluteFilePath).replace(/\./g, ''),
          path: relative(FILES_VAULT_DIRECTORY, absoluteFilePath),
          tags: [],
        },
      );
    } catch (e) {
      this.logger.error('error updating file: ', e);
    }
  };

  /**
   * Retries reading file from disk.
   */
  private retryReadFile = async (absoluteFilePath: string) => {
    // will make 69 retries which in total take 31 hours of attempts
    // to calculate total time spent on retries (in hours) use this function:
    // const calculateTotalTimeSpentOnRetries = (MAX_RETRIES) => Math.round(Array.from('1'.repeat(69).split('')).map((el, index) => (index + 1)**2).reduce((sum, next) => sum + next,0) /60 /60)
    const MAX_RETRIES = 69;

    let retries = 0;
    let fileBuffer;

    while (retries <= MAX_RETRIES) {
      try {
        fileBuffer = await readFile(absoluteFilePath);

        if (retries > 0) {
          this.logger.log(
            `file read successfully after ${retries} retries: ${absoluteFilePath}`,
          );
        }

        break;
      } catch (e) {
        retries++;
        this.logger.warn(
          `error reading file: ${absoluteFilePath}. Retrying... ${retries}`,
        );
        await scheduler.wait(retries * retries * 1000);
      }
    }

    return fileBuffer;
  };

  private getAudioMetadata = async (fileBuffer: Buffer, mimeType: string) => {
    const metadataFull = await parseBuffer(fileBuffer, mimeType);
    let metadata: TAudioMetadataClientModel = {
      format: {},
      common: {
        disk: {
          no: null,
          of: null,
        },
        track: {
          no: null,
          of: null,
        },
      },
    };

    if (metadataFull) {
      const {
        common = {} as IAudioMetadata['common'],
        format = {} as IAudioMetadata['format'],
      } = metadataFull;

      metadata = {
        format: {
          bitrate: format.bitrate,
          duration: format.duration,
          lossless: format.lossless,
        },
        common: {
          album: common.album,
          albumartist: common.albumartist,
          artist: common.artist,
          artists: common.artists,
          disk: common.disk,
          genre: common.genre,
          title: common.title,
          track: common.track,
          year: common.year,
        },
      };
    }

    return {
      metadata,
      metadataFull,
    };
  };

  private getVideoMetadata = async (filePath: string) => {
    const metadataFull = (await ffprobeAsync(filePath)) || ({} as FfprobeData);
    let metadata: TVideoMetadataClientModel = {
      format: {},
      streams: [],
    };

    if (metadataFull) {
      const { format = {}, streams = [] } = metadataFull;

      metadata = {
        format: {
          size: format.size,
          duration: format.duration,
        },
        streams: streams?.map((stream) => ({
          index: stream.index,
          codec_name: stream.codec_name,
          codec_type: stream.codec_type,
          width: stream.width,
          height: stream.height,
          duration: stream.duration,
        })),
      };
    }

    return {
      metadata,
      metadataFull,
    };
  };

  private getImageMetadata = async (fileBuffer: Buffer) => {
    const metadataFull = await sharp(fileBuffer).metadata();
    let metadata: TImageMetadataClientModel = {};

    if (metadataFull) {
      metadata = {
        format: metadataFull.format,
        width: metadataFull.width,
        height: metadataFull.height,
        size: metadataFull.size,
      };
    }

    return {
      metadata,
      metadataFull,
    };
  };

  private getFileMetadata = async (
    fileBuffer: Buffer,
    filePath: string,
    mimeType: string,
  ) => {
    const type = mimeType.split('/')[0];
    let result;

    try {
      switch (type) {
        case 'audio':
          result = await this.getAudioMetadata(fileBuffer, mimeType);
          break;
        case 'video':
          result = await this.getVideoMetadata(filePath);
          break;
        case 'image':
          result = await this.getImageMetadata(fileBuffer);
          break;
        default:
          result = {
            metadata: {},
            metadataFull: {},
          };
          break;
      }
    } catch (e) {
      this.logger.error('error getting file metadata: ', e);
      result = {
        metadata: {},
        metadataFull: {},
      };
    }

    return {
      metadata: JSON.stringify(result.metadata),
      metadataFull: JSON.stringify(result.metadataFull),
    };
  };

  /**
   * Search if there's an entry for the file with given absolute path.
   * Creates new entry if non found.
   */
  private findOrCreateFileEntity = async (
    absoluteFilePath: string,
    user?: UserEntity,
  ): Promise<FileEntity> => {
    let fileEntry;
    const masterUser = await this.userRepository.findOneBy({
      isMaster: true,
    });
    const allUsers = await this.userRepository.find();
    const owner = getOwner(absoluteFilePath, allUsers, masterUser, user);
    const savePath = getFilePath(absoluteFilePath, masterUser, owner);

    try {
      fileEntry = await this.fileRepository.findOneBy({
        path: savePath,
      });
    } catch (e) {
      this.logger.error('error finding file entity: ', e);
    }

    if (!fileEntry) {
      let fileBuffer;

      try {
        fileBuffer = await this.retryReadFile(absoluteFilePath);
      } catch (e) {
        this.logger.error('error reading file: ', e);
      }

      try {
        if (fileBuffer) {
          const { mime, ext } = (await fromBuffer(fileBuffer)) || {};
          const { metadata, metadataFull } = await this.getFileMetadata(
            fileBuffer,
            absoluteFilePath,
            mime,
          );

          fileEntry = this.fileRepository.create({
            uuid: randomUUID(),
            name: path.basename(absoluteFilePath),
            hash: createHash('sha256').update(fileBuffer).digest('hex'),
            size: Buffer.byteLength(fileBuffer),
            mimeType: mime,
            extension: ext || path.extname(absoluteFilePath).replace(/\./g, ''),
            path: savePath,
            tags: [],
            metadata,
            metadataFull,
            owner,
          });

          const savedFileEntry = await this.fileRepository.save(fileEntry);

          this.recordInitialScanStatistics(owner, savedFileEntry);

          return await this.fileRepository.save(fileEntry);
        }
      } catch (e) {
        this.logger.error('error creating file entity: ', e);
      }
    } else {
      return fileEntry;
    }
  };

  private findOrCreateDirectoryEntity = async (
    absoluteDirectoryPath: string,
  ): Promise<DirectoryEntity> => {
    let directoryEntry;
    const owner = getOwner(
      absoluteDirectoryPath,
      await this.userRepository.find(),
      await this.userRepository.findOneBy({
        isMaster: true,
      }),
    );

    try {
      directoryEntry = await this.directoryRepository.findOneBy({
        path: relative(FILES_VAULT_DIRECTORY, absoluteDirectoryPath),
      });
    } catch (e) {
      this.logger.error('error finding directory entity: ', e);
    }

    if (!directoryEntry) {
      try {
        directoryEntry = this.directoryRepository.create({
          uuid: randomUUID(),
          path: relative(FILES_VAULT_DIRECTORY, absoluteDirectoryPath),
          owner,
        });

        return await this.directoryRepository.save(directoryEntry);
      } catch (e) {
        this.logger.error('error creating directory entity: ', e);
      }
    } else {
      return directoryEntry;
    }
  };

  private recordInitialScanStatistics = (
    owner: UserEntity,
    savedFileEntry: FileEntity,
  ) => {
    if (!this.initialScanComplete) {
      this.initialScanStatistics = {
        ...this.initialScanStatistics,
        [owner.email]: {
          ...(this.initialScanStatistics[owner.email] || {}),
          added: [
            ...(this.initialScanStatistics[owner.email]?.added || []),
            savedFileEntry,
          ],
        },
      };
    }
  };

  /**
   * Saves file on physical disk.
   *
   * Returns absolute file path.
   */
  private saveFileOnDisk = async (
    newFile: Express.Multer.File,
    user: UserEntity,
  ): Promise<string> => {
    const fullFilePath = path.join(
      FILES_VAULT_DIRECTORY,
      user.email,
      newFile.fieldname,
    );
    const fullFileDirectory = path.parse(fullFilePath).dir;

    await mkdirp(fullFileDirectory);
    await writeFile(fullFilePath, newFile.buffer);

    return fullFilePath;
  };
}
