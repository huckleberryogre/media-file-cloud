import {
  Body,
  Controller,
  Delete,
  Get,
  Injectable,
  Logger,
  Param,
  Patch,
  Post,
  Req,
  Sse,
  StreamableFile,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FilesService } from './files.service';
import { UpdateFileDto } from './dto/update-file.dto';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { Observable, Subscriber } from 'rxjs';
import { EventEmitter2 } from '@nestjs/event-emitter';
import type { Express, Request } from 'express';
import path from 'path';

import _ from 'lodash';
import { createReadStream } from 'node:fs';
import { FILES_VAULT_DIRECTORY } from '../../config';
import { IFeedback } from '../../models.shared';
import { FEEDBACK_TEXTS } from './feedback.texts';
import { EEntityVisibility } from '../../enums';
import { EVENTS } from '../../constants';
import { Public } from '../auth/decorators/public.decorator';

const { throttle } = _;

@Injectable()
@Controller('api/files')
export class FilesController {
  private readonly logger = new Logger(FilesController.name);
  private observables: Record<string, Observable<any>> = {};
  private recalculateMetadataRequests = {};

  constructor(
    private readonly filesService: FilesService,
    private eventEmitter: EventEmitter2,
  ) {}

  /**
   * Creates new file.
   */
  @Post('/upload')
  @UseInterceptors(AnyFilesInterceptor())
  async create(
    @UploadedFiles() files: Express.Multer.File[],
    @Req() req: Request,
  ) {
    let feedback: IFeedback;

    try {
      await this.filesService.handleUploadedFiles(files, req.user);
      feedback = {
        success: FEEDBACK_TEXTS.upload.success,
      };
    } catch (error) {
      this.logger.error(error);
      feedback = {
        error: FEEDBACK_TEXTS.upload.error.generic,
      };
    }

    return { feedback };
  }

  @Post('/set-visibility')
  async setVisibility(
    @Body()
    { uuid, visibility }: { uuid: string; visibility: EEntityVisibility },
    @Req() req: Request,
  ) {
    let feedback: IFeedback;
    try {
      await this.filesService.update(uuid, req.user, {
        visibility,
      });
      feedback = {
        success: FEEDBACK_TEXTS.file.visibility.success(visibility),
      };
    } catch (error) {
      this.logger.error(error);
      feedback = {
        error: FEEDBACK_TEXTS.file.visibility.error.generic,
      };
    }

    return { feedback };
  }

  /**
   * Returns all files entries.
   */
  @Get('entries')
  findAll(@Req() req: Request) {
    return this.filesService.findAll(req.user);
  }

  /**
   * Returns shared file entry.
   */
  @Public()
  @Get('shared/:uuid')
  findSharedFileEntry(@Param('uuid') uuid: string) {
    return this.filesService.findSharedFilesEntries(uuid);
  }

  /**
   * Returns directories.
   */
  @Get('directories')
  findDirectories(@Req() req: Request) {
    return this.filesService.findDirectories(req.user);
  }

  /**
   * Sends files changes to the client.
   *
   * todo: sse is not websocket, it is very limited in connections count which cause problems when multiple tabs are open.
   * todo: better switch to websockets. See this thread https://stackoverflow.com/questions/18584525/server-sent-events-and-browser-limits
   * todo: also check if sessionID and express-session package are still required here and set up correctly
   */
  @Sse('files-changed')
  filesChanged(@Req() request: Request) {
    const user = request.user;

    // todo: for the EVENTS.FILES_ENTRIES_UPDATE event, add check if file change is related to the current user. Right now every user will get an update on every file change.
    const handleFilesChange = throttle((subscriber: Subscriber<any>) => {
      this.logger.debug(
        `Files changed for user ${user.email}. Sending update to the client with SSE.`,
      );

      this.filesService
        .findAll(user)
        // subscriber.next is sending provided data back to the client side
        .then((files) => subscriber.next({ data: files }));
    }, 1000);

    if (!this.observables[request.sessionID]) {
      this.observables[request.sessionID] = new Observable((subscriber) => {
        const listener = () => {
          handleFilesChange(subscriber);
        };

        this.eventEmitter.on(EVENTS.FILES_ENTRIES_UPDATE, listener);

        return () => {
          this.eventEmitter.off(EVENTS.FILES_ENTRIES_UPDATE, listener);
        };
      });
    }

    return this.observables[request.sessionID];
  }

  /**
   * Returns file by uuid.
   */
  @Get(':uuid')
  async findOne(@Param('uuid') uuid: string) {
    const file = await this.filesService.findOneFileEntry(uuid);

    const fileStream = createReadStream(
      path.join(FILES_VAULT_DIRECTORY, file.path),
    );

    const encodedFileName = encodeURIComponent(file.name);

    return new StreamableFile(fileStream, {
      type: file.mimeType,
      disposition: `attachment; filename*=UTF-8''${encodedFileName}`,
      length: file.size,
    });
  }

  /**
   * Updates file by id with given DTO.
   */
  @Patch(':uuid')
  update(
    @Param('uuid') uuid: string,
    @Body() updateFileDto: UpdateFileDto,
    @Req() req: Request,
  ) {
    return this.filesService.update(uuid, req.user, updateFileDto);
  }

  /**
   * Removes file by id.
   */
  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      const feedback = await this.filesService.unlinkFile({
        where: { id: +id },
      });
      return { feedback };
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Recalculates all user's files metadata.
   */
  @Post('/recalculate-metadata')
  recalculateMetadata(@Req() req: Request) {
    let request;

    if (!this.recalculateMetadataRequests[req.user.email]) {
      request = this.filesService
        .recalculateFilesMetadata(req.user)
        .then(() => {
          delete this.recalculateMetadataRequests[req.user.email];
        });
      this.recalculateMetadataRequests[req.user.email] = request;
    } else {
      request = this.recalculateMetadataRequests[req.user.email];
    }

    return request;
  }
}
