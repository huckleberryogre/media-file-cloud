import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
} from 'typeorm';
import { BaseEntity } from '../../../enitites/base.entity';
import { TagEntity } from '../../tags/entities/tag.entity';
import { UserEntity } from '../../users/entities/user.entity';
import { Exclude, Transform } from 'class-transformer';
import { EEntityVisibility } from '../../../enums';

@Entity()
export class FileEntity extends BaseEntity {
  /**
   * UUID for the file.
   */
  @Column({ unique: true })
  uuid: string;

  /**
   * Original file name (basename).
   * @example my_photo.jpg
   */
  @Column()
  name: string;

  /**
   * File hash.
   */
  @Column()
  hash: string;

  /**
   * File metadata that goes to the client (part of full metadata).
   */
  @Column({ nullable: true })
  @Transform(({ value }) => {
    let parsedValue;

    try {
      parsedValue = JSON.parse(value);
    } catch (e) {
      console.error(e);
      parsedValue = {};
    }

    return parsedValue;
  })
  metadata: string;

  /**
   * Full file metadata parsed from the file.
   */
  @Exclude()
  @Column({ nullable: true })
  metadataFull: string;

  /**
   * File size in bytes.
   */
  @Column()
  size: number;

  /**
   * File mimetype.
   */
  @Column({ nullable: true })
  mimeType?: string;

  /**
   * File extension.
   */
  @Column({ nullable: true })
  extension?: string;

  /**
   * Full file path [including file name],
   * e.g. for windows: C:\hello\file.txt.
   */
  @Column()
  @Transform(({ obj }: { value: string; obj: FileEntity }) => {
    const transformedFilePath = obj.path
      .split(/[\\/]/)
      .filter((path) => path && path !== obj.name);

    // if user is not master, remove the first path element
    // which is the user's email
    if (!obj.owner?.isMaster) {
      transformedFilePath.shift();
    }

    return transformedFilePath;
  })
  path: string;

  /**
   * File tags.
   */
  @ManyToMany(() => TagEntity, (tag) => tag.files, { eager: true })
  @JoinTable()
  @Transform(({ value }) => value.map(({ name, id }) => ({ name, id })))
  tags: TagEntity[];

  /**
   * File owner email.
   */
  @ManyToOne(() => UserEntity, (user) => user.files, { eager: true })
  @JoinColumn()
  @Exclude()
  owner: UserEntity;

  /**
   * File visibility.
   */
  @Column({ default: EEntityVisibility.PRIVATE })
  visibility: EEntityVisibility;
}
