import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../enitites/base.entity';
import { UserEntity } from '../../users/entities/user.entity';
import { Exclude } from 'class-transformer';
import { EEntityVisibility } from '../../../enums';

@Entity()
export class DirectoryEntity extends BaseEntity {
  /**
   * UUID for the directory.
   */
  @Column({ unique: true })
  uuid: string;

  /**
   * Full directory path
   * e.g. for windows: C:\hello\
   */
  @Column()
  path: string;

  /**
   * Directory owner email.
   */
  @ManyToOne(() => UserEntity, (user) => user.directories, { eager: true })
  @JoinColumn()
  @Exclude()
  owner: UserEntity;

  /**
   * Directory visibility.
   */
  @Column({ default: EEntityVisibility.PRIVATE })
  visibility: EEntityVisibility;
}
