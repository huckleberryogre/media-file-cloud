import path, { relative } from 'path';
import { FILES_VAULT_DIRECTORY } from '../../../config';
import { UserEntity } from '../../users/entities/user.entity';

/**
 * Calculates the final relative path for the file to be saved.
 */
export const getFilePath = (
  absoluteOriginalFilePath: string,
  masterUser: UserEntity,
  owner: UserEntity,
) => {
  let result: string;

  const relativeOriginalFilePath = relative(
    FILES_VAULT_DIRECTORY,
    absoluteOriginalFilePath,
  );

  const isLegalOwner = relativeOriginalFilePath.startsWith(owner.email);
  const isMasterUser = owner.email === masterUser.email;

  if (isLegalOwner || isMasterUser) {
    // default scenario
    result = relativeOriginalFilePath;
  } else {
    // fallback to master user owner
    result = path.join(masterUser.email, relativeOriginalFilePath);
  }

  return result;
};
