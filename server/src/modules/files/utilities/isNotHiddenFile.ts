import { FileEntity } from '../entities/file.entity';

/**
 * Filter function for hidden files.
 * Hidden files/folders are files that start with a dot.
 * @example .gitignore, .vscode, .env, .DS_Store, .database
 *
 * @param file File entity.
 */
export const isNotHiddenFile = (file: FileEntity) =>
  !file.name.startsWith('.') &&
  !file.path.split('/').some((dir) => dir.startsWith('.'));
