import { isNotHiddenFile } from '../isNotHiddenFile';
import { FileEntity } from '../../entities/file.entity';

describe('isNotHiddenFile', () => {
  it('should return true for a file that is not hidden', () => {
    const file = {
      id: 1,
      name: 'file.txt',
      path: '/hello/file.txt',
    };
    expect(isNotHiddenFile(file as FileEntity)).toBeTruthy();
  });
  it('should return false for a file that is hidden', () => {
    const file = {
      id: 1,
      name: '.file.txt',
      path: '/hello/.file.txt',
    };
    expect(isNotHiddenFile(file as FileEntity)).toBeFalsy();
  });
  it('should return false for a file that is in a hidden directory', () => {
    const file = {
      id: 1,
      name: 'file.txt',
      path: '/hello/.hidden/file.txt',
    };
    expect(isNotHiddenFile(file as FileEntity)).toBeFalsy();
  });
});
