import { relative } from 'path';
import { FILES_VAULT_DIRECTORY } from '../../../config';
import { UserEntity } from '../../users/entities/user.entity';

/**
 * Calculates owner from given path. If owner is not provided, it will try to find the owner from the path.
 * If owner is provided, it will check if the path starts with the owner email. If it does, it will use the owner.
 */
export const getOwner = (
  absoluteOriginalFilePath: string,
  allUsers: UserEntity[],
  masterUser: UserEntity,
  owner?: UserEntity,
) => {
  let result: UserEntity;

  const relativeOriginalFilePath = relative(
    FILES_VAULT_DIRECTORY,
    absoluteOriginalFilePath,
  );

  if (owner?.email) {
    if (relativeOriginalFilePath.startsWith(owner.email)) {
      result =
        allUsers.find(({ email }) => email === owner.email) || masterUser;
    } else {
      result = masterUser;
    }
  } else {
    const foundUser = allUsers.find(({ email }) =>
      relativeOriginalFilePath.startsWith(email),
    );

    result = foundUser || masterUser;
  }

  return result;
};
