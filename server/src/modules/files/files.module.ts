import { Module } from '@nestjs/common';
import { FilesService } from './files.service';
import { FilesController } from './files.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileEntity } from './entities/file.entity';
import { UserEntity } from '../users/entities/user.entity';
import { DirectoryEntity } from './entities/directory.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([FileEntity, UserEntity, DirectoryEntity]),
  ],
  controllers: [FilesController],
  providers: [FilesService],
  exports: [FilesService],
})
export class FilesModule {}
