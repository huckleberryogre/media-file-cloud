import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Post,
  Req,
} from '@nestjs/common';
import { TagsService } from './tags.service';
import { Request } from 'express';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { EVENTS } from '../../constants';

@Controller('api/tags')
export class TagsController {
  private readonly logger = new Logger(TagsController.name);

  constructor(
    private readonly tagsService: TagsService,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  @Post('update')
  async updateTags(
    @Body() { tags, fileUUID }: { tags: string[]; fileUUID: string },
    @Req() req: Request,
  ) {
    try {
      await this.tagsService.updateTagsForFile(fileUUID, tags, req.user);
    } catch (error) {
      this.logger.error('Error updating tags: ', error);
      this.eventEmitter.emit(EVENTS.NOTIFICATION, {
        error: 'Error updating tags',
      });
    }
  }

  @Get()
  findAll(@Req() req: Request) {
    return this.tagsService.findAll(req.user);
  }

  @Delete(':id')
  async remove(@Param('id') id: string, @Req() req: Request) {
    try {
      await this.tagsService.remove(+id, req.user);

      this.eventEmitter.emit(EVENTS.NOTIFICATION, {
        success: 'Tag deleted',
      });
    } catch (error) {
      this.logger.error('Error deleting tag: ', error);
      this.eventEmitter.emit(EVENTS.NOTIFICATION, {
        error: 'Error deleting tag',
      });
    }
  }
}
