import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TagEntity } from './entities/tag.entity';
import { FileEntity } from '../files/entities/file.entity';
import { Repository } from 'typeorm';
import { UserEntity } from '../users/entities/user.entity';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { EVENTS } from '../../constants';

@Injectable()
export class TagsService {
  private readonly logger = new Logger(TagsService.name);

  constructor(
    @InjectRepository(TagEntity)
    private readonly tagsRepository: Repository<TagEntity>,
    @InjectRepository(FileEntity)
    private readonly filesRepository: Repository<FileEntity>,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  /**
   * Upserts tag.
   * @param name Tag name.
   * @param user User who made the request.
   */
  upsertTag = async (name: string, user: UserEntity): Promise<TagEntity> => {
    let tag = await this.tagsRepository.findOne({
      where: { name, user: { id: user.id } },
    });

    if (!tag) {
      try {
        tag = await this.tagsRepository.save({ name, user });
      } catch (error) {
        this.logger.error('Upsert tag error: ', error);
      }
      return tag;
    } else {
      return tag;
    }
  };

  /**
   * Updates tags for file.
   */
  updateTagsForFile = async (
    fileUUID: string,
    tags: string[],
    user: UserEntity,
  ) => {
    const upsertedTags = await Promise.allSettled(
      tags.map((tag) => this.upsertTag(tag, user)),
    );
    const resultTags = upsertedTags
      .filter((tagRecord) => tagRecord.status === 'fulfilled')
      .map(
        (tagRecord) => (tagRecord as PromiseFulfilledResult<TagEntity>).value,
      );

    const file = await this.filesRepository.findOne({
      where: {
        uuid: fileUUID,
        owner: {
          id: user.id,
        },
      },
    });

    file.tags = resultTags;
    await this.filesRepository.save(file);

    this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
  };

  async findAll(user: UserEntity) {
    return await this.tagsRepository
      .find({
        where: {
          user: {
            id: user.id,
          },
        },
        relations: ['files'],
      })
      .then((tags) =>
        tags.map((tag) => ({
          ...tag,
          files: tag.files.map((file) => file.uuid),
        })),
      );
  }

  async remove(id: number, user: UserEntity) {
    const tag = await this.tagsRepository.findOne({
      where: {
        id,
        user: {
          id: user.id,
        },
      },
    });

    await this.tagsRepository.remove(tag);

    this.eventEmitter.emit(EVENTS.FILES_ENTRIES_UPDATE);
  }
}
