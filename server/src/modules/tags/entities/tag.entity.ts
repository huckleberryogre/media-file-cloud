import { Column, Entity, ManyToMany, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../enitites/base.entity';
import { FileEntity } from '../../files/entities/file.entity';
import { UserEntity } from '../../users/entities/user.entity';

@Entity()
export class TagEntity extends BaseEntity {
  /**
   * Tag name.
   * @example 'cats'
   */
  @Column()
  name: string;

  /**
   * Files that have this tag.
   */
  @ManyToMany(() => FileEntity, (file) => file.tags)
  files: FileEntity[];

  /**
   * User who created the tag.
   */
  @ManyToOne(() => UserEntity, (user) => user.tags)
  user: UserEntity;
}
