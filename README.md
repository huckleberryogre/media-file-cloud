# Media file cloud

[WIP] A simple file cloud for all of your media. Easy store and share files from your own server machine. Integrate with other apps through convenient API.

* Client and server parts are in the respective folders. See readme of each for more details.
* [docker](https://www.docker.com/) is required in order to run the app

## Launch 🚀

### Step #1: build a docker image

```shell
npm run build
```

### Step #2: spin it

```shell
npm run start
```

### App is available on http://localhost:4000

## App features 👈 

* 📦 containerized with docker
* 🔑 Authentication system with JWT login/password or Google account**
* #️⃣ database mapping the file vault contents from destination folder**
* ️*️⃣ all CRUD operations for files are available from the client (including bulk files/folder upload)
* 🔄 all manual file system operations are immediately synchronised with database and client
* 🧪 utilities unit tests on both client and server sides

** find available configurations in the `server/.env.sample` file. Place your own `.env` file in the `server` folder.