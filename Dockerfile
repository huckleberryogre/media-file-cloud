ARG NODE_VERSION

FROM node:${NODE_VERSION}-alpine

RUN apk update && apk add g++ make py3-pip \
     && yarn global add @nestjs/cli

WORKDIR /app
COPY server/package.json server/package.json
COPY server/yarn.lock server/yarn.lock
COPY client/package.json client/package.json
COPY client/yarn.lock client/yarn.lock

RUN cd server && yarn install --production
RUN cd client && yarn install

COPY client client
COPY server server

RUN cd server && yarn run build
RUN cd client && yarn run build

WORKDIR /app/server
CMD ["node", "dist/main.js"]
EXPOSE 4000
